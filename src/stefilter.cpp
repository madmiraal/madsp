// (c) 2016 - 2017: Marcel Admiraal

#include "stefilter.h"

namespace ma
{
    STEFilter::STEFilter(const unsigned int duration,
            const unsigned int channels, const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, duration)
    {
    }

    STEFilter::STEFilter(const TemporalData& temporalData) :
        data(temporalData)
    {
    }

    STEFilter::STEFilter(const STEFilter& source) :
        data(source.data)
    {
    }

    STEFilter::~STEFilter()
    {
    }

    void swap(STEFilter& first, STEFilter& second)
    {
        swap(first.data, second.data);
    }

    STEFilter& STEFilter::operator=(STEFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double STEFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        data.addData(input, channel, milliseconds);
        return data.getData(channel, false).elementSquaredSum() /
                data.getDataLength();
    }

    Filter* STEFilter::createCopy() const
    {
        STEFilter* aCopy = new STEFilter(*this);
        return aCopy;
    }

    unsigned int STEFilter::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int STEFilter::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int STEFilter::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str STEFilter::getDescription() const
    {
        Str result = "Short-term energy filter (Duration: " +
                toStr(data.getDataLength() * data.getUpdatePeriod()) + " ms)";
        return result;
    }

    TemporalData STEFilter::getTemporalData() const
    {
        return data;
    }

    std::istream& operator>>(std::istream& is, STEFilter& destination)
    {
        TemporalData temporalData;
        is >> temporalData;
        if (is.good())
        {
            destination = STEFilter(temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const STEFilter& source)
    {
        os << source.getTemporalData();
        return os;
    }
}
