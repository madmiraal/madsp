// (c) 2016 - 2017: Marcel Admiraal

#include "difffilter.h"

#include "butter.h"
#include "strstream.h"

namespace ma
{
    DiffFilter::DiffFilter() :
        aLength(0), bLength(0), tLength(0), channels(0)
    {
        initialise();
    }

    DiffFilter::DiffFilter(const Vector& a, const Vector& b,
            const unsigned int channels, const unsigned int sampleFrequency) :
        aLength(a.size()), bLength(b.size()),
        tLength(3 * aLength > bLength ? aLength : bLength),
        channels(channels), sampleFrequency(sampleFrequency),
        a(a), b(b)
    {
        initialise();
    }

    DiffFilter::DiffFilter(const unsigned int order,
            const unsigned int cutoffFrequency, const bool high,
            const unsigned int channels, const unsigned int sampleFrequency) :
        aLength(order+1), bLength(order+1), tLength(order+1),
        channels(channels), sampleFrequency(sampleFrequency)
    {
        butter(order, cutoffFrequency, sampleFrequency, b, a, high);
        initialise();
    }

    DiffFilter::DiffFilter(const unsigned int order,
            const unsigned int lowCutoffFrequency,
            const unsigned int highCutoffFrequency, bool stop,
            const unsigned int channels, const unsigned int sampleFrequency) :
        aLength(2*order+1), bLength(2*order+1), tLength(2*order+1),
        channels(channels), sampleFrequency(sampleFrequency)
    {
        butter(order, lowCutoffFrequency, highCutoffFrequency, sampleFrequency,
                b, a, stop);
        initialise();
    }

    DiffFilter::DiffFilter(const DiffFilter& source) :
        aLength(source.aLength), bLength(source.bLength),
        tLength(source.tLength), channels(source.channels),
        sampleFrequency(source.sampleFrequency),
        a(source.a), b(source.b)
    {
        initialise();
    }

    DiffFilter::~DiffFilter()
    {
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            delete[] inputHistory[channel];
            delete[] outputHistory[channel];
        }
        delete[] inputHistory;
        delete[] outputHistory;
        delete[] lastUpdate;
    }

    void swap(DiffFilter& first, DiffFilter& second)
    {
        std::swap(first.aLength, second.aLength);
        std::swap(first.bLength, second.bLength);
        std::swap(first.tLength, second.tLength);
        std::swap(first.channels, second.channels);
        std::swap(first.sampleFrequency, second.sampleFrequency);
        std::swap(first.inputHistory, second.inputHistory);
        std::swap(first.outputHistory, second.outputHistory);
        swap(first.a, second.a);
        swap(first.b, second.b);
        std::swap(first.lastUpdate, second.lastUpdate);
    }

    DiffFilter& DiffFilter::operator=(DiffFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double DiffFilter::filter(const double newInput, const unsigned int channel,
            const unsigned int milliseconds)
    {
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());

        unsigned int updates = 1000 * milliseconds / sampleFrequency;
        if (milliseconds == 0) updates = (now - lastUpdate[channel]).count();
        if (updates == 0) return outputHistory[channel][tLength - 1];

        double delta = (newInput - inputHistory[channel][tLength - 1]) / updates;
        for (unsigned int update = 0; update < updates; ++update)
        {
            double thisInput = inputHistory[channel][tLength - 1] + delta;
            shiftHistory(channel);
            inputHistory[channel][tLength - 1] = thisInput;
            outputHistory[channel][tLength - 1] = calculateDifference(
                    a.array(), aLength, b.array(), bLength,
                    inputHistory[channel], outputHistory[channel], tLength);
        }

        lastUpdate[channel] = now;

        return outputHistory[channel][tLength - 1];
    }

    Filter* DiffFilter::createCopy() const
    {
        DiffFilter* aCopy = new DiffFilter(*this);
        return aCopy;
    }

    unsigned int DiffFilter::getChannels() const
    {
        return channels;
    }

    unsigned int DiffFilter::getSampleFrequency() const
    {
        return sampleFrequency;
    }

    unsigned int DiffFilter::getSamplePeriod() const
    {
        return 1000 / sampleFrequency;
    }

    Str DiffFilter::getDescription() const
    {
        StrStream strStream;
        strStream << "Difference filter (a: ";
        print(a, strStream);
        strStream << ", b: ";
        print(b, strStream);
        strStream << ")";
        return strStream.toStr();
    }

    Vector DiffFilter::getA() const
    {
        return a;
    }

    Vector DiffFilter::getB() const
    {
        return b;
    }

    void DiffFilter::initialise()
    {
        inputHistory = new double*[channels];
        outputHistory = new double*[channels];
        lastUpdate = new std::chrono::milliseconds[channels];
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            inputHistory[channel] = new double[tLength]();
            outputHistory[channel] = new double[tLength]();
            lastUpdate[channel] =
                    std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::steady_clock::now().time_since_epoch());
        }
    }

    void DiffFilter::shiftHistory(const unsigned int channel)
    {
        for (unsigned int index = 1; index < tLength; ++index)
        {
            inputHistory[channel][index-1] = inputHistory[channel][index];
            outputHistory[channel][index-1] = outputHistory[channel][index];
        }
    }

    double calculateDifference(const double a[], const unsigned int aLength,
            const double b[], const unsigned int bLength,
            const double inputHistory[], const double outputHistory[],
            const unsigned int tLength)
    {
        // a[0]*y[n] = b[0]*x[n] + b[1]*x[n-1] + ... + b[nb]*x[n-nb]
        //                       - a[1]*y[n-1] - ... - a[na]*y[n-na]
        double difference = 0;
        for (unsigned int i = 0; i < bLength; ++i)
            difference += b[i] * inputHistory[tLength - 1 - i];
        for (unsigned int i = 1; i < aLength; ++i)
            difference -= a[i] * outputHistory[tLength - 1 - i];
        difference /= a[0];
        return difference;
    }

    std::istream& operator>>(std::istream& is, DiffFilter& destination)
    {
        Vector a, b;
        unsigned int channels, sampleFrequency;
        is >> a >> b;
        is >> channels >> sampleFrequency;
        if (is.good())
        {
            destination = DiffFilter(a, b, channels, sampleFrequency);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const DiffFilter& source)
    {
        os << source.getA() << '\t';
        os << source.getB() << '\t';
        os << source.getChannels() << '\t';
        os << source.getSampleFrequency();
        return os;
    }
}
