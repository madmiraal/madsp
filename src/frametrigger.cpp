// (c) 2016 - 2017: Marcel Admiraal

#include "frametrigger.h"

#include "vector.h"
#include "strstream.h"

namespace ma
{
    FrameTrigger::FrameTrigger(const double minValue, const unsigned int minTime,
            const double maxValue, const unsigned int maxTime,
            const unsigned int channels, const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, maxTime + 1),
        minValue(minValue), minTime(minTime),
        maxValue(maxValue), minIndex(maxTime - minTime + 1)
    {
    }

    FrameTrigger::FrameTrigger(const double minValue, const unsigned int minTime,
            const double maxValue, const unsigned int maxTime,
            const TemporalData& data) :
        data(data), minValue(minValue), minTime(minTime),
        maxValue(maxValue), minIndex(maxTime - minTime + 1)
    {
    }

    FrameTrigger::FrameTrigger(const FrameTrigger& source) :
        data(source.data), minValue(source.minValue), minTime(source.minTime),
        maxValue(source.maxValue), minIndex(source.minIndex)
    {
    }

    FrameTrigger::~FrameTrigger()
    {
    }


    void swap(FrameTrigger& first, FrameTrigger& second)
    {
        swap(first.data, second.data);
        std::swap(first.minValue, second.minValue);
        std::swap(first.minTime, second.minTime);
        std::swap(first.maxValue, second.maxValue);
        std::swap(first.minIndex, second.minIndex);
    }

    FrameTrigger& FrameTrigger::operator=(FrameTrigger source)
    {
        swap(*this, source);
        return *this;
    }

    double FrameTrigger::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        data.addData(input, channel, milliseconds);
        return testFrame(data.getData(channel, false)) ? 1 : 0;
    }

    Filter* FrameTrigger::createCopy() const
    {
        FrameTrigger* aCopy = new FrameTrigger(*this);
        return aCopy;
    }

    unsigned int FrameTrigger::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int FrameTrigger::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int FrameTrigger::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str FrameTrigger::getDescription() const
    {
        StrStream strStream;
        strStream << "Frame trigger (Between: " << minValue;
        strStream << " and " << maxValue;
        strStream << ", for: " << minTime;
        strStream << " to " << (minIndex + minTime - 1);
        strStream << " ms)";
        return strStream.toStr();
    }

    TemporalData FrameTrigger::getTemporalData() const
    {
        return data;
    }

    unsigned int FrameTrigger::getMinTime() const
    {
        return minTime;
    }

    double FrameTrigger::getMinValue() const
    {
        return minValue;
    }

    unsigned int FrameTrigger::getMaxTime() const
    {
        // minIndex = maxTime - minTime + 1
        // maxTime = minIndex + minTime - 1
        return minIndex + minTime - 1;
    }

    double FrameTrigger::getMaxValue() const
    {
        return maxValue;
    }

    bool FrameTrigger::testFrame(const Vector& historicData)
    {
        // Retrieve minimum and maximum brackets.
        Vector minBracket = historicData.getSubvector(minIndex, minTime);
        Vector maxBracket = historicData.getSubvector(0, minIndex - 1);

        // Check minimum time thresholds.
        if (minBracket.min() < minValue) return false;
        if (minBracket.max() > maxValue) return false;
        // Check maximum time thresholds.
        if (maxBracket.min() > minValue && maxBracket.max() < maxValue)
                return false;

        return true;
    }

    std::istream& operator>>(std::istream& is, FrameTrigger& destination)
    {
        TemporalData temporalData;
        double minValue;
        unsigned int minTime;
        double maxValue;
        unsigned int maxTime;
        is >> temporalData;
        is >> minValue;
        is >> minTime;
        is >> maxValue;
        is >> maxTime;
        if (is.good())
        {
            destination = FrameTrigger(minValue, minTime, maxValue, maxTime,
                    temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const FrameTrigger& source)
    {
        os << source.getTemporalData() << '\t';
        os << source.getMinValue() << '\t';
        os << source.getMinTime() << '\t';
        os << source.getMaxValue() << '\t';
        os << source.getMaxTime();
        return os;
    }
}
