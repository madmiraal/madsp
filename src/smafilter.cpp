// (c) 2016 - 2017: Marcel Admiraal

#include "smafilter.h"

namespace ma
{
    SMAFilter::SMAFilter(const unsigned int duration,
            const unsigned int channels, const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, duration)
    {
    }

    SMAFilter::SMAFilter(const TemporalData& temporalData) :
        data(temporalData)
    {
    }

    SMAFilter::SMAFilter(const SMAFilter& source) :
        data(source.data)
    {
    }

    SMAFilter::~SMAFilter()
    {
    }

    void swap(SMAFilter& first, SMAFilter& second)
    {
        swap(first.data, second.data);
    }

    SMAFilter& SMAFilter::operator=(SMAFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double SMAFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        data.addData(input, channel, milliseconds);
        return data.getData(channel, false).mean();
    }

    Filter* SMAFilter::createCopy() const
    {
        SMAFilter* aCopy = new SMAFilter(*this);
        return aCopy;
    }

    unsigned int SMAFilter::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int SMAFilter::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int SMAFilter::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str SMAFilter::getDescription() const
    {
        Str result = "Simple moving average filter (Duration: " +
                toStr(data.getDataLength() * data.getUpdatePeriod()) + " ms)";
        return result;
    }

    TemporalData SMAFilter::getTemporalData() const
    {
        return data;
    }

    std::istream& operator>>(std::istream& is, SMAFilter& destination)
    {
        TemporalData temporalData;
        is >> temporalData;
        if (is.good())
        {
            destination = SMAFilter(temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const SMAFilter& source)
    {
        os << source.getTemporalData();
        return os;
    }
}
