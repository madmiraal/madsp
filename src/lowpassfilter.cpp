// (c) 2016 - 2017: Marcel Admiraal

#include "lowpassfilter.h"

#include <cmath>

namespace ma
{
    LowPassFilter::LowPassFilter(const unsigned int cutoff,
            const unsigned int channels,
            const unsigned int sampleFrequency) :
        RC(1.0 / (2 * M_PI * cutoff)), cutoff(cutoff), channels(channels),
        sampleFrequency(sampleFrequency)
    {
        initialise();
    }

    LowPassFilter::LowPassFilter(const double RC, const unsigned int channels,
            const unsigned int sampleFrequency)
        : RC(RC), cutoff(1.0 / (2 * M_PI * RC) + .5), channels(channels),
        sampleFrequency(sampleFrequency)
    {
        initialise();
    }

    LowPassFilter::LowPassFilter(const LowPassFilter& source) :
        RC(source.RC), cutoff(source.cutoff), channels(source.channels),
        sampleFrequency(source.sampleFrequency)
    {
        initialise();
    }

    LowPassFilter::~LowPassFilter()
    {
        delete[] filteredValue;
    }

    void swap(LowPassFilter& first, LowPassFilter& second)
    {
        std::swap(first.RC, second.RC);
        std::swap(first.filteredValue, second.filteredValue);
        std::swap(first.cutoff, second.cutoff);
        std::swap(first.channels, second.channels);
        std::swap(first.sampleFrequency, second.sampleFrequency);
        std::swap(first.previousTime, second.previousTime);
    }

    LowPassFilter& LowPassFilter::operator=(LowPassFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double LowPassFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        // Calculate time since last input.
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        double dt = milliseconds / 1000.;
        if (dt == 0) dt = (now - previousTime).count() / 1000.;
        previousTime = now;

        // Filter data.
        double alpha = dt / (RC + dt);
        filteredValue[channel] += alpha * (input - filteredValue[channel]);
        return filteredValue[channel];
    }

    Filter* LowPassFilter::createCopy() const
    {
        LowPassFilter* aCopy = new LowPassFilter(*this);
        return aCopy;
    }

    unsigned int LowPassFilter::getChannels() const
    {
        return channels;
    }

    unsigned int LowPassFilter::getSampleFrequency() const
    {
        return sampleFrequency;
    }

    unsigned int LowPassFilter::getSamplePeriod() const
    {
        return 1000 / sampleFrequency;
    }

    Str LowPassFilter::getDescription() const
    {
        Str result = "Low-pass filter (Cutoff: " + toStr(cutoff) + " Hz)";
        return result;
    }

    double LowPassFilter::getRC() const
    {
        return RC;
    }

    void LowPassFilter::initialise()
    {
        filteredValue = new double[channels]();
        previousTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
    }

    std::istream& operator>>(std::istream& is, LowPassFilter& destination)
    {
        unsigned int channels;
        double RC;
        is >> channels;
        is >> RC;
        if (is.good())
        {
            destination = LowPassFilter(RC, channels);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const LowPassFilter& source)
    {
        os << source.getChannels() << '\t';
        os << source.getRC();
        return os;
    }
}
