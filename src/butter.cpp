// (c) 2017: Marcel Admiraal

#include "butter.h"

#include <cmath>

namespace ma
{
    // Given a low pass filter represented by poles and zeros in the
    // splane, they can be converted to a low pass, high pass, band pass or
    // band stop filter by transforming each of the poles and zeros.
    //
    // Transform         Zero at x                  Pole at x
    // ----------------  -------------------------  ------------------------
    // Low Pass          zero: Fc x/C               pole: Fc x/C
    // S -> C S/Fc       gain: C/Fc                 gain: Fc/C
    // ----------------  -------------------------  ------------------------
    // High Pass         zero: Fc C/x               pole: Fc C/x
    // S -> C Fc/S       pole: 0                    zero: 0
    //                   gain: -x                   gain: -1/x
    // ----------------  -------------------------  ------------------------
    // Band Pass         zero: b +- sqrt(b^2-FhFl)  pole: b +- sqrt(b^2-FhFl)
    //        S^2+FhFl   pole: 0                    zero: 0
    // S -> C --------   gain: C/(Fh-Fl)            gain: (Fh-Fl)/C
    //        S(Fh-Fl)   b=x/C (Fh-Fl)/2            b=x/C (Fh-Fl)/2
    // ----------------  -------------------------  ------------------------
    // Band Stop         zero: b +- sqrt(b^2-FhFl)  pole: b +- sqrt(b^2-FhFl)
    //        S(Fh-Fl)   pole: +-sqrt(-FhFl)        zero: +-sqrt(-FhFl)
    // S -> C --------   gain: -x                   gain: -1/x
    //        S^2+FhFl   b=C/x (Fh-Fl)/2            b=C/x (Fh-Fl)/2
    // ----------------  -------------------------  ------------------------
    // Bilinear          zero: (2+xT)/(2-xT)        pole: (2+xT)/(2-xT)
    //      2 z-1        pole: -1                   zero: -1
    // S -> - ---        gain: (2-xT)/T             gain: (2-xT)/T
    //      T z+1
    // ----------------  -------------------------  ------------------------
    //
    // C is the cutoff frequency of the initial lowpass filter, Fc is
    // the edge of the target low/high pass filter and [Fl,Fh] are the edges
    // of the target band pass/stop filter.  With abundant tedious algebra,
    // you can derive the above formulae yourself by substituting the
    // transform for S into H(S)=S-x for a zero at x or H(S)=1/(S-x) for a
    // pole at x, and converting the result into the form:
    //
    //    H(S)=g prod(S-Xi)/prod(S-Xj)
    //
    // Please note that a pole and a zero at the same place exactly cancel.
    // This is significant for High Pass, Band Pass and Band Stop filters
    // which create numerous extra poles and zeros, most of which cancel.
    // Those which do not cancel have a "fill-in" effect, extending the
    // shorter of the sets to have the same number of as the longer of the
    // sets of poles and zeros (or at least split the difference in the case
    // of the band pass filter).  There may be other opportunistic
    // cancellations but I will not check for them.
    //
    // Also note that any pole on the unit circle or beyond will result in
    // an unstable filter.  Because of cancellation, this will only happen
    // if the number of poles is smaller than the number of zeros and the
    // filter is high pass or band pass.  The analytic design methods all
    // yield more poles than zeros, so this will not be a problem.
    //
    // Based on Octave code:
    // (c) 1999-2001 Paul Kienzle <pkienzle@users.sf.net>

    // Low or high-pass filter.
    void sftrans(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double Fc, const bool stop)
    {
        double C = 1;
        unsigned int p = pole.size();
        unsigned int z = zero.size();
        if (z > p || p == 0)
            throw "Must have at least as many poles as zeros in s-plane";

        // ----------------  -------------------------  ------------------------
        // High-pass         zero: Fc C/x               pole: Fc C/x
        // S -> C Fc/S       pole: 0                    zero: 0
        //                   gain: -x                   gain: -1/x
        // ----------------  -------------------------  ------------------------
        if (stop)
        {
            gain *= ((-zero).elementProduct() /
                     (-pole).elementProduct()).real();

            pole = C * Fc / pole;

            zero = C * Fc / zero;
            if (p > z)
            {
                zero.extend(p-z);
            }
        }

        // ----------------  -------------------------  ------------------------
        // Low-pass          zero: Fc x/C               pole: Fc x/C
        // S -> C S/Fc       gain: C/Fc                 gain: Fc/C
        // ----------------  -------------------------  ------------------------
        else
        {
            for (unsigned int i = 0; i < p-z; ++i)
                gain /= C / Fc;

            pole = Fc * pole / C;

            zero = Fc * zero / C;
        }
    }

    // Band-pass or stop filter.
    void sftrans(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double Fl, const double Fh, const bool stop)
    {
        double C = 1;
        unsigned int p = pole.size();
        unsigned int z = zero.size();
        if (z > p || p == 0)
            throw "Must have at least as many poles as zeros in s-plane";

        // ----------------  -------------------------  ------------------------
        // Band-stop         zero: b ± sqrt(b^2-FhFl)   pole: b ± sqrt(b^2-FhFl)
        //        S(Fh-Fl)   pole: ±sqrt(-FhFl)         zero: ±sqrt(-FhFl)
        // S -> C --------   gain: -x                   gain: -1/x
        //        S^2+FhFl   b=C/x (Fh-Fl)/2            b=C/x (Fh-Fl)/2
        // ----------------  -------------------------  ------------------------
        if (stop)
        {
            gain *= ((-zero).elementProduct()/
                     (-pole).elementProduct()).real();

            VectorComplex bpole = (C*(Fh-Fl)/2)/pole;
            pole.extend(p);
            for (unsigned int i = 0; i < p; ++i)
            {
                Complex base = bpole[i]*bpole[i] - Fh*Fl;
                Complex sqrt[2];
                root(base, 2, sqrt);
                pole[i]   = bpole[i] + sqrt[0];
                pole[i+p] = bpole[i] + sqrt[1];
            }

            VectorComplex bzero = (C*(Fh-Fl)/2)/zero;
            zero.extend(z);
            for (unsigned int i = 0; i < z; ++i)
            {
                Complex base = bzero[i]*bzero[i] - Fh*Fl;
                Complex sqrt[2];
                root(base, 2, sqrt);
                zero[i*2]   = bzero[i] + sqrt[0];
                zero[i*2+1] = bzero[i] + sqrt[1];
            }
            if (p > z)
            {
                zero.extend(2*(p-z));
                Complex base = -Fh*Fl;
                Complex sqrt[2];
                root(base, 2, sqrt);
                for (unsigned int i = 0; i < p-z; ++i)
                {
                    zero[2*z + i*2]   = sqrt[0];
                    zero[2*z + i*2+1] = sqrt[1];
                }
            }
        }

        // ----------------  -------------------------  ------------------------
        // Band-pass         zero: b ± sqrt(b^2-FhFl)   pole: b ± sqrt(b^2-FhFl)
        //        S^2+FhFl   pole: 0                    zero: 0
        // S -> C --------   gain: C/(Fh-Fl)            gain: (Fh-Fl)/C
        //        S(Fh-Fl)   b=x/C (Fh-Fl)/2            b=x/C (Fh-Fl)/2
        // ----------------  -------------------------  ------------------------
        else
        {
            for (unsigned int i = 0; i < p-z; ++i)
                gain /= C/(Fh-Fl);
            VectorComplex bpole = pole*((Fh-Fl)/(2*C));
            pole.extend(p);
            for (unsigned int i = 0; i < p; ++i)
            {
                Complex base = bpole[i]*bpole[i] - Fh*Fl;
                Complex sqrt[2];
                root(base, 2, sqrt);
                pole[i]   = bpole[i] + sqrt[0];
                pole[i+p] = bpole[i] + sqrt[1];
            }

            VectorComplex bzero = zero*((Fh-Fl)/(2*C));
            zero.extend(z);
            for (unsigned int i = 0; i < z; ++i)
            {
                Complex base = bzero[i]*bzero[i] - Fh*Fl;
                Complex sqrt[2];
                root(base, 2, sqrt);
                pole[i]   = bzero[i] + sqrt[0];
                pole[i+p] = bzero[i] + sqrt[1];
            }
            if (p > z)
            {
                zero.extend(p-z);
            }
        }
    }

    // Given a piecewise flat filter design, it can be transformed
    // from the s-plane to the z-plane while maintaining the band edges by
    // means of the bilinear transform.  This maps the left hand side of the
    // s-plane into the interior of the unit circle.  The mapping is highly
    // non-linear, so the filter must be designed with band edges in the
    // s-plane positioned at 2/T tan(w*T/2) so that they will be positioned
    // at w after the bilinear transform is complete.
    //
    // +---------------+-----------------------+----------------------+
    // | Transform     | Zero at x             | Pole at x            |
    // |    H(S)       |   H(S) = S-x          |    H(S)=1/(S-x)      |
    // +---------------+-----------------------+----------------------+
    // |       2 z-1   | zero: (2+xT)/(2-xT)   | zero: -1             |
    // |  S -> - ---   | pole: -1              | pole: (2+xT)/(2-xT)  |
    // |       T z+1   | gain: (2-xT)/T        | gain: (2-xT)/T       |
    // +---------------+-----------------------+----------------------+
    //
    // The above formula can be derived by substituting the transform for S into
    // H(S)=S-x for a zero at x or H(S)=1/(S-x) for a pole at x, and converting
    // the result into the form:
    //
    //    H(Z)=g prod(Z-Xi)/prod(Z-Xj)
    //
    // A pole and a zero at the same place exactly cancel. This is significant
    // since the bilinear transform creates numerous extra poles and zeros, most
    // of which cancel. Those which do not cancel have a "fill-in" effect,
    // extending the shorter of the sets to have the same number of as the
    // longer of the sets of poles and zeros (or at least split the difference
    // in the case of the band pass filter). There may be other opportunistic
    // cancellations but they are not checked for.
    //
    // Any pole on the unit circle or beyond will result in an unstable filter.
    // Because of cancellation, this will only happen if the number of poles is
    // smaller than the number of zeros. The analytic design methods all yield
    // more poles than zeros, so this will not be a problem.
    //
    // Based on Octave code:
    // (c) 1999 Paul Kienzle <pkienzle@users.sf.net>
    void bilinear(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double T)
    {
        unsigned int p = pole.size();
        unsigned int z = zero.size();
        if (z > p || p==0)
            throw "Must have at least as many poles as zeros in s-plane";

        // ----------------  -------------------------  ------------------------
        // Bilinear          zero: (2+xT)/(2-xT)        pole: (2+xT)/(2-xT)
        //      2 z-1        pole: -1                   zero: -1
        // S -> - ---        gain: (2-xT)/T             gain: (2-xT)/T
        //      T z+1
        // ----------------  -------------------------  ------------------------
        gain *= (((Complex(2)-zero*T)/T).elementProduct() /
                 ((Complex(2)-pole*T)/T).elementProduct()).real();

        pole = (Complex(2) + pole * T) / (Complex(2) - pole * T);

        zero = (Complex(2) + zero * T) / (Complex(2) - zero * T);
        zero.extend(p-z, -1);
    }

    // Based on Octave code:
    // (c) 1994-2015 John W. Eaton
    // Based on code by KH <Kurt.Hornik@wu-wien.ac.at>
    VectorComplex poly(const VectorComplex& root)
    {
        unsigned int n = root.size();
        VectorComplex y = complexZeros(n+1);
        y[0] = 1;
        for (unsigned int i = 0; i < n; ++i)
        {
            for (unsigned int j = i+1; j > 0; --j)
            {
                y[j] -= root[i] * y[j-1];
            }
        }
        return y;
    }

    // Based on Octave code:
    // (c) 1999 Paul Kienzle <pkienzle@users.sf.net>
    // (c) 2003 Doug Stewart <dastew@sympatico.ca>
    // (c) 2011 Alexander Klein <alexander.klein@math.uni-giessen.de>

    // Low or high-pass filter.
    void butter(const unsigned int order, const unsigned int frequency,
            const unsigned int sample,
            Vector& a, Vector& b, const bool high)
    {
        VectorComplex zero, pole;
        double gain;
        butter(order, frequency, sample, zero, pole, gain, high);

        a = (gain * poly(zero)).real();
        b = (poly(pole)).real();
    }

    // Band-pass or stop filter.
    void butter(const unsigned int order, const unsigned int low,
            const unsigned int high, const unsigned int sample,
            Vector& a, Vector& b, const bool stop)
    {
        VectorComplex zero, pole;
        double gain;
        butter(order, low, high, sample, zero, pole, gain, stop);

        a = (gain * poly(zero)).real();
        b = (poly(pole)).real();
    }

    // Low or high-pass filter.
    void butter(const unsigned int order, const unsigned int frequency,
            const unsigned int sample,
            VectorComplex& zero, VectorComplex& pole, double& gain,
            const bool high)
    {
        zero = VectorComplex();
        gain = 1;

        double w = (double)frequency / (sample / 2);
        if (w > 1)
            throw "Cutoff frequency must less than half the sample frequency.";

        // Prewarp to the band edges to s plane using sampling frequency of 2 Hz.
        double T = 2;
        w = 2 / T * std::tan (M_PI * w / T);

        // Generate splane poles for the prototype Butterworth filter.
        pole = VectorComplex(order);
        for (unsigned int i = 0; i < order; ++i)
        {
            //pole = 1.0 * exp (1i * pi * (2 * [1:order] + order - 1) / (2 * order));
            pole[i] = exp(Complex(0, 1) * M_PI * (2 * (i+1) + order - 1) /
                          (2 * order));
        }
        if (order % 2 == 1)
        {
            // Pure real value at exp(i*pi)
            pole[(order + 1) / 2 - 1] = -1;
        }

        // Splane frequency transform.
        sftrans (zero, pole, gain, w, high);

        // Use bilinear transform to convert poles to the z plane.
        bilinear (zero, pole, gain, T);
    }

    // Band-pass or stop filter.
    void butter(const unsigned int order, const unsigned int low,
            const unsigned int high, const unsigned int sample,
            VectorComplex& zero, VectorComplex& pole, double& gain,
            const bool stop)
    {
        zero = VectorComplex();
        gain = 1;

        if (high <= low)
            throw "High frequency must be greater than low frequency.";
        double wl = (double)low / (sample / 2);
        double wh = (double)high / (sample / 2);
        if (wh > 1)
            throw "High frequency must less than half the sample frequency.";

        // Prewarp to the band edges to s plane using sampling frequency of 2 Hz.
        double T = 2;
        wl = 2 / T * std::tan (M_PI * wl / T);
        wh = 2 / T * std::tan (M_PI * wh / T);

        // Generate splane poles for the prototype Butterworth filter.
        pole = VectorComplex(order);
        for (unsigned int i = 0; i < order; ++i)
        {
            //pole = 1.0 * exp (1i * pi * (2 * [1:order] + order - 1) / (2 * order));
            pole[i] = exp(Complex(0, 1) * M_PI * (2 * (i+1) + order - 1) /
                          (2 * order));
        }
        if (order % 2 == 1)
        {
            // Pure real value at exp(i*pi)
            pole[(order + 1) / 2 - 1] = -1;
        }

        // Splane frequency transform.
        sftrans (zero, pole, gain, wl, wh, stop);

        // Use bilinear transform to convert poles to the z plane.
        bilinear (zero, pole, gain, T);
    }
}

// Reference:
// Proakis & Manolakis (1992). Digital Signal Processing. New York:
// Macmillan Publishing Company.

// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, see <http://www.gnu.org/licenses/>.
