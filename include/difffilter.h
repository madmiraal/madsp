// Direct Form II implementation of the standard difference equation:
// a[0]*y[n] = b[0]*x[n] + b[1]*x[n-1] + ... + b[nb]*x[n-nb]
//                       - a[1]*y[n-1] - ... - a[na]*y[n-na]
//
// Includes constructors for creating any order low, high, band-pass or
// band-stop Butterworth filters.
// (c) 2016 - 2017: Marcel Admiraal

#ifndef DIFFFILTER_H
#define DIFFFILTER_H

#include "filter.h"
#include "temporaldata.h"

#include <chrono>

namespace ma
{
    class DiffFilter : public Filter
    {
    public:
        /**
         * Default constructor.
         */
        DiffFilter();

        /**
         * Butterworth parameter constructor.
         *
         * @param a                 The a parameter vector.
         * @param b                 The b parameter vector.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default: 1000Hz
         */
        DiffFilter(const Vector& a, const Vector& b,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Low or high-pass Butterworth difference filter constructor.
         *
         * @param order             The order of the Butterworth filter to
         *                          generate.
         * @param cutoffFrequency   The cutoff frequency in Hz.
         * @param high              Whether or not this is a high-pass filter.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default: 1000Hz
         */
        DiffFilter(const unsigned int order, const unsigned int cutoffFrequency,
                const bool high, const unsigned int sampleFrequency = 1000,
                const unsigned int channels = 1);

        /**
         * Band-pass or stop Butterworth difference filter constructor.
         *
         * @param order     The order of the Butterworth filter to generate.
         * @param lowCutoffFrequency    The low frequency in Hz.
         * @param highCutoffFrequency   The high frequency in Hz.
         * @param stop                  Whether or not this is a band-stop
         *                              filter.
         * @param channels              The number of data channels. Default: 1
         * @param sampleFrequency       The expected sample frequency.
         *                              Default: 1000Hz
         */
        DiffFilter(const unsigned int order,
                const unsigned int lowCutoffFrequency,
                const unsigned int highCutoffFrequency, const bool stop,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Copy constructor.
         *
         * @param source    The difference filter to copy.
         */
        DiffFilter(const DiffFilter& source);

        /**
         * Default destructor.
         */
        virtual ~DiffFilter();

        /**
         * Swap function.
         *
         * @param first  The first difference filter to swap with.
         * @param second The second difference filter to swap with.
         */
        friend void swap(DiffFilter& first, DiffFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the difference filter.
         *
         * @param source The difference filter to copy.
         * @return       A copy of the difference filter.
         */
        DiffFilter& operator=(DiffFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns a copy of the a vector.
         *
         * @return A copy of the a vector.
         */
        Vector getA() const;

        /**
         * Returns a copy of the b vector.
         *
         * @return a copy of the b vector.
         */
        Vector getB() const;

    private:
        void initialise();
        void shiftHistory(const unsigned int channel);

        unsigned int aLength, bLength, tLength;
        unsigned int channels, sampleFrequency;
        double** inputHistory;
        double** outputHistory;
        Vector a, b;
        std::chrono::milliseconds* lastUpdate;
    };

    /**
     * Calculates the difference.
     *
     * @param a             The array of a values.
     * @param aLength       The length of the a array.
     * @param b             The array of b values.
     * @param bLenth        The length of the b array.
     * @param inputHistory  The array with the input history.
     * @param outputHistory The array with the output history.
     * @param tLength       The length of the history arrays.
     */
    double calculateDifference(const double a[], const unsigned int aLength,
            const double b[], const unsigned int bLength,
            const double inputHistory[], const double outputHistory[],
            const unsigned int tLength);

    /**
     * Input stream operator.
     * Builds a difference filter from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The difference filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, DiffFilter& destination);

    /**
     * Output stream operator.
     * Appends the difference filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The difference filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const DiffFilter& source);
}

#endif // DIFFFILTER_H
