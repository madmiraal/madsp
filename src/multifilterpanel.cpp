// (c) 2017: Marcel Admiraal

#include "multifilterpanel.h"

#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/choicdlg.h>
#include <wx/filedlg.h>
#include <wx/msgdlg.h>

#include "filterfactory.h"

#include <fstream>

namespace ma
{
    MultiFilterPanel::MultiFilterPanel(wxWindow* parent,
            MultiFilter* multiFilter) :
        wxPanel(parent), multiFilter(multiFilter)
    {
        wxBoxSizer* filterPanelSizer = new wxBoxSizer(wxHORIZONTAL);

        filterList = new wxListCtrl(this, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxLC_REPORT | wxLC_NO_HEADER);
        filterList->AppendColumn(wxEmptyString, wxLIST_FORMAT_LEFT, 430);
        populateFilterList();
        filterPanelSizer->Add(filterList, 1, wxALL, 5);

        wxSizer* filterButtonSizer = new wxBoxSizer(wxVERTICAL);
        wxButton* addFilterButton = new wxButton(this, wxID_ANY,
                wxT("Add Filter"));
        addFilterButton->Bind(wxEVT_BUTTON,
                &MultiFilterPanel::addFilter, this);
        filterButtonSizer->Add(addFilterButton, 0, wxALL, 5);
        wxButton* clearFiltersButton = new wxButton(this, wxID_ANY,
                wxT("Clear Filters"));
        clearFiltersButton->Bind(wxEVT_BUTTON,
                &MultiFilterPanel::clearFilters, this);
        filterButtonSizer->Add(clearFiltersButton, 0, wxALL, 5);
        wxButton* loadFiltersButton = new wxButton(this, wxID_ANY,
                wxT("Load Filters"));
        loadFiltersButton->Bind(wxEVT_BUTTON,
                &MultiFilterPanel::loadFilters, this);
        filterButtonSizer->Add(loadFiltersButton, 0, wxALL, 5);
        wxButton* saveFiltersButton = new wxButton(this, wxID_ANY,
                wxT("Save Filters"));
        saveFiltersButton->Bind(wxEVT_BUTTON,
                &MultiFilterPanel::saveFilters, this);
        filterButtonSizer->Add(saveFiltersButton, 0, wxALL, 5);

        filterPanelSizer->Add(filterButtonSizer, 0);

        SetSizerAndFit(filterPanelSizer);
    }

    MultiFilterPanel::~MultiFilterPanel()
    {
    }

    void MultiFilterPanel::setMultiFilter(MultiFilter* multiFilter)
    {
        this->multiFilter = multiFilter;
        populateFilterList();
    }

    void MultiFilterPanel::populateFilterList()
    {
        filterList->DeleteAllItems();
        unsigned int filters = multiFilter->filterCount();
        Str descriptions[filters];
        multiFilter->descriptions(descriptions);
        for (unsigned int filter = 0; filter < filters; ++filter)
        {
            filterList->InsertItem(filter, descriptions[filter].c_str());
        }
    }

    void MultiFilterPanel::addFilter(wxCommandEvent& event)
    {
        Filter* newFilter = 0;
        wxSingleChoiceDialog filterChoice(this, wxT("Select filter to add:"),
                wxT("Add Filter"),
                FilterFactory::getFilterTypeCount(),
                FilterFactory::getFilterTypeNames());
        if (filterChoice.ShowModal() == wxID_CANCEL) return;
        newFilter = FilterFactory::createFilter(this,
                filterChoice.GetSelection(), multiFilter->getChannels(),
                multiFilter->getSampleFrequency());
        if (newFilter && multiFilter->addFilter(newFilter))
        {
            unsigned int filterIndex = multiFilter->filterCount() - 1;
            filterList->InsertItem(filterIndex,
                    newFilter->getDescription().c_str());
            filterList->EnsureVisible(filterIndex);
            delete newFilter;
        }
    }

    void MultiFilterPanel::clearFilters(wxCommandEvent& event)
    {
        multiFilter->clearFilters();
        filterList->DeleteAllItems();
    }

    void MultiFilterPanel::loadFilters(wxCommandEvent& event)
    {
        wxString filename("multifilter.dat");
        wxString windowTitle("Load Multi-Filter");
        bool success = false;
        while (!success)
        {
            wxFileDialog openFileDialog(this, windowTitle, wxEmptyString,
                    filename, wxFileSelectorDefaultWildcardStr,
                    wxFD_OPEN|wxFD_FILE_MUST_EXIST);
            if (openFileDialog.ShowModal() == wxID_CANCEL) return;
            std::ifstream fileStream(openFileDialog.GetPath().c_str());
            if (fileStream.good())
            {
                MultiFilter loadedFilter;
                fileStream >> loadedFilter;
                if (fileStream.good())
                {
                    *multiFilter = loadedFilter;
                    populateFilterList();
                    success = true;
                }
            }
            if (!success)
            {
                wxMessageBox(wxT("Load Failed"));
            }
        }
    }

    void MultiFilterPanel::saveFilters(wxCommandEvent& event)
    {
        wxString filename("multifilter.dat");
        wxString windowTitle("Save Multi-Filter");
        bool success = false;
        while (!success)
        {
            wxFileDialog saveFileDialog(this, windowTitle, wxEmptyString,
                    filename, wxFileSelectorDefaultWildcardStr,
                    wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
            if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
            std::ofstream fileStream(saveFileDialog.GetPath().c_str());
            if (fileStream.good())
            {
                fileStream << *multiFilter << std::endl;
                if (fileStream.good()) success = true;
            }
            if (!success)
            {
                wxMessageBox(wxT("Save Failed"));
            }
        }
    }

    MultiFilterDialog::MultiFilterDialog(ma::MultiFilter* multiFilter,
            wxWindow* parent, wxWindowID id, const wxString& title,
            const wxPoint& pos, const wxSize& size, long style,
            const wxString& name) :
        wxDialog(parent, id, title, pos, size, style, name)
    {
        wxBoxSizer* dialogSizer = new wxBoxSizer(wxVERTICAL);
        dialogSizer->Add(new MultiFilterPanel(this, multiFilter), 1, wxEXPAND);
        dialogSizer->Add(CreateButtonSizer(wxOK | wxCANCEL), 0, wxCENTER);
        SetSizerAndFit(dialogSizer);
    }
}
