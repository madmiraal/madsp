// The filter abstract class that all filters are derived from.
// The default constructor for all filters should be a null filter i.e.
// the output equals the input.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef FILTER_H
#define FILTER_H

#include "vector.h"
#include "str.h"

namespace ma
{
    class Filter
    {
    public:
        /**
         * Virtual destructor.
         */
        virtual ~Filter() {};

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0) = 0;

        /**
         * Filters data for all channels in real-time.
         *
         * @param input         The vector with all the channels' new data.
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The vector with the filtered data.
         */
       virtual Vector filterAll(const Vector& input,
                const unsigned int milliseconds = 0);

        /**
         * Filters static time-series data.
         *
         * @param input The vector with the time-series data to be filtered.
         * @return      The vector with the filtered data.
         */
       virtual Vector filterStatic(const Vector& input) const;

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const = 0;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const = 0;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const = 0;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const = 0;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const = 0;
    };

    /**
     * Filters static time-series data.
     *
     * @param filter    Pointer to the filter to use.
     * @param input     The vector with the time-series data to be filtered.
     * @return          The vector with the filtered data.
     */
    Vector filterStatic(const Filter* filter, const Vector& input);
}

#endif // FILTER_H
