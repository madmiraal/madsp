// An envelope filter using the magnitude of the analytic signal to obtain the
// instantaneous amplitude.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef ENVELOPEFILTER_H
#define ENVELOPEFILTER_H

#include "filter.h"
#include "temporaldata.h"
#include "complex.h"

namespace ma
{
    class EnvelopeFilter : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param duration          The duration in milliseconds to average over.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        EnvelopeFilter(const unsigned int duration = 1,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Temporal data structure constructor.
         *
         * @param temporalData  The temporal data structure to copy.
         */
        EnvelopeFilter(const TemporalData& temporalData);

        /**
         * Copy constructor.
         *
         * @param source    The envelope filter to copy.
         */
        EnvelopeFilter(const EnvelopeFilter& source);

        /**
         * Default destructor.
         */
        virtual ~EnvelopeFilter();

        /**
         * Swap function.
         *
         * @param first  The first envelope filter to swap with.
         * @param second The second envelope filter to swap with.
         */
        friend void swap(EnvelopeFilter& first, EnvelopeFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the envelope filter.
         *
         * @param source The envelope filter to copy.
         * @return       A copy of the envelope filter.
         */
        EnvelopeFilter& operator=(EnvelopeFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns a copy of the underlying temporal data structure.
         *
         * @return A copy of the underlying temporal data structure.
         */
        TemporalData getTemporalData() const;

    private:
        unsigned int length;
        TemporalData data;
    };

    /**
     * Calculates the instantaneous magnitude at each point.
     *
     * @param data          The data array.
     * @param length        The length of the array.
     */
    void instantaneousMagnitude(double data[], const unsigned int length);

    /**
     * If the duration is not of length 2^n increase it to the next 2^n.
     *
     * @param length The minimum length required.
     */
    unsigned int getLogableLength(const unsigned int length);

    /**
     * Input stream operator.
     * Builds a envelope filter from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The envelope filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, EnvelopeFilter& destination);

    /**
     * Output stream operator.
     * Appends the envelope filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The envelope filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const EnvelopeFilter& source);
}

#endif // ENVELOPEFILTER_H
