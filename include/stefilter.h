// A short-term energy filter.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef STEFILTER_H
#define STEFILTER_H

#include "filter.h"
#include "temporaldata.h"

namespace ma
{
    class STEFilter : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param duration          The duration in milliseconds to average over.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        STEFilter(const unsigned int duration = 1,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Temporal data structure constructor.
         *
         * @param temporalData  The temporal data structure to copy.
         */
        STEFilter(const TemporalData& temporalData);

        /**
         * Copy constructor.
         *
         * @param source    The short-term energy filter to copy.
         */
        STEFilter(const STEFilter& source);

        /**
         * Default destructor.
         */
        virtual ~STEFilter();

        /**
         * Swap function.
         *
         * @param first  The first short-term energy filter to swap with.
         * @param second The second short-term energy filter to swap with.
         */
        friend void swap(STEFilter& first, STEFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the short-term energy filter.
         *
         * @param source The short-term energy filter to copy.
         * @return       A copy of the short-term energy filter.
         */
        STEFilter& operator=(STEFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns a copy of the underlying temporal data structure.
         *
         * @return A copy of the underlying temporal data structure.
         */
        TemporalData getTemporalData() const;

    private:
        TemporalData data;
    };

    /**
     * Input stream operator.
     * Builds a short-term energy filter from an input stream and returns the
     * input stream.
     *
     * @param is            The input stream.
     * @param destination   The short-term energy filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, STEFilter& destination);

    /**
     * Output stream operator.
     * Appends the short-term energy filter to the output stream and returns the
     * output stream.
     *
     * @param os     The output stream.
     * @param source The short-term energy filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const STEFilter& source);
}

#endif // STEFILTER_H
