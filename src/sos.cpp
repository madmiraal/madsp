// (c) 2017: Marcel Admiraal

#include "sos.h"

namespace ma
{
    // Based on Octave code:
    // (c) 2005 Julius O. Smith III <jos@ccrma.stanford.edu>
    Matrix zp2sos(const VectorComplex& zero, const VectorComplex& pole,
            const double gain)
    {
        Vector zr = zero.getReal();
        VectorComplex zc = zero.getPosImag();
        Vector pr = pole.getReal();
        VectorComplex pc = pole.getPosImag();

        unsigned int nzr=zr.size();
        unsigned int nzc=zc.size();
        unsigned int npr=pr.size();
        unsigned int npc=pc.size();

        // Pair up real zeros.
        if (nzr % 2 == 1)
        {
            zr.extend(1);
            ++nzr;
        }
        unsigned int nzrsec = nzr / 2;
        Vector zrms(nzrsec);
        Vector zrp(nzrsec);
        for (unsigned int i = 0; i < nzrsec; ++i)
        {
            zrms[i] = -zr[i*2] - zr[i*2+1];
            zrp[i]  =  zr[i*2] * zr[i*2+1];
        }

        // Pair up real poles.
        if (npr % 2 == 1)
        {
            pr.extend(1);
            ++npr;
        }
        unsigned int nprsec = npr / 2;
        Vector prms(nprsec);
        Vector prp(nprsec);
        for (unsigned int i = 0; i < nprsec; ++i)
        {
            prms[i] = -pr[i*2] - pr[i*2+1];
            prp[i]  =  pr[i*2] * pr[i*2+1];
        }

        unsigned int nsecs = nzc+nzrsec > npc+nprsec ? nzc+nzrsec : npc+nprsec;

        // Convert complex zeros and poles to real 2nd-order section form.
        Vector zcm2r = -2 * zc.real();
        Vector zca2 = zc.abs() * zc.abs();
        Vector pcm2r = -2 * pc.real();
        Vector pca2 = pc.abs() * pc.abs();

        Matrix sos = zeros(nsecs, 6);
        // All 2nd-order polynomials are monic.
        for (unsigned int i = 0; i < nsecs; ++i)
        {
            sos[i][0] = 1;
            sos[i][3] = 1;
        }

        // Index of last real zero section.
        unsigned int nzrl = nzc + nzrsec;
        // Index of last real pole section.
        unsigned int nprl = npc + nprsec;

        for (unsigned int i = 0; i < nsecs; ++i)
        {
            if (i < nzc)
            {
                // Lay down a complex zero pair.
                sos[i][1] = zcm2r[i];
                sos[i][2] = zca2[i];
            }
            else if (i < nzrl)
            {
                // Lay down a pair of real zeros.
                sos[i][1] = zrms[i-nzc];
                sos[i][2] = zrp[i-nzc];
            }

            if (i < npc)
            {
                // Lay down a complex pole pair.
                sos[i][4] = pcm2r[i];
                sos[i][5] = pca2[i];
            }
            else if (i < nprl)
            {
                // Lay down a pair of real poles.
                sos[i][4] = prms[i-npc];
                sos[i][5] = prp[i-npc];
            }
        }

        // Combine gain into the first section.
        for (unsigned int i = 0; i < 3; ++i)
            sos[0][i] *= gain;

        return sos;
    }
}

// This program is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program; if not, see <http://www.gnu.org/licenses/>.
