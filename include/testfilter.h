// Sam's simple filter.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef TESTFILTER_H
#define TESTFILTER_H

#include "filter.h"
#include "temporaldata.h"

#include <chrono>

namespace ma
{
    class TestFilter : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        TestFilter(const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Temporal data structure constructor.
         *
         * @param temporalData  The temporal data structure to copy.
         */
        TestFilter(const TemporalData& temporalData);

        /**
         * Copy constructor.
         *
         * @param source    The test filter to copy.
         */
        TestFilter(const TestFilter& source);

        /**
         * Default destructor.
         */
        virtual ~TestFilter();

        /**
         * Swap function.
         *
         * @param first  The first test filter to swap with.
         * @param second The second test filter to swap with.
         */
        friend void swap(TestFilter& first, TestFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the test filter.
         *
         * @param source The test filter to copy.
         * @return       A copy of the test filter.
         */
        TestFilter& operator=(TestFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the filter's channels.
         *
         * @return The filter's channels.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns a copy of the underlying temporal data structure.
         *
         * @return A copy of the underlying temporal data structure.
         */
        TemporalData getTemporalData() const;

    private:
        void initialise(const unsigned int channels);

        TemporalData data;
        double* x;
        double* z;
    };

    /**
     * Input stream operator.
     * Builds a test filter from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The test filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, TestFilter& destination);

    /**
     * Output stream operator.
     * Appends the test filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The test filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const TestFilter& source);
}

#endif // TESTFILTER_H
