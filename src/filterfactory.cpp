// (c) 2017: Marcel Admiraal

#include "filterfactory.h"

#include <wx/msgdlg.h>
#include <wx/textdlg.h>

#include "smafilter.h"
#include "medianfilter.h"
#include "binarytrigger.h"
#include "stefilter.h"
#include "frametrigger.h"
#include "lowpassfilter.h"
#include "highpassfilter.h"
#include "envelopefilter.h"
#include "testfilter.h"
#include "difffilter.h"
#include "multifilter.h"
#include "multifilterpanel.h"

namespace ma
{
    const unsigned int FilterFactory::getFilterTypeCount()
    {
        return filterTypeCount;
    }

    const wxString* FilterFactory::getFilterTypeNames()
    {
        return filterTypeName;
    }

    Filter* FilterFactory::createFilter(wxWindow* parent,
            const unsigned int index, const unsigned int channels,
            const unsigned int sampleFrequency)
    {
        Filter* newFilter = 0;
        switch (index)
        {
        case 0:
            newFilter = createMovingAverageFilter(parent, channels,
                    sampleFrequency);
            break;
        case 1:
            newFilter = createMedianFilter(parent, channels,
                    sampleFrequency);
            break;
        case 2:
            newFilter = createBinaryTrigger(parent, channels,
                    sampleFrequency);
            break;
        case 3:
            newFilter = createShortTermEnergyFilter(parent, channels,
                    sampleFrequency);
            break;
        case 4:
            newFilter = createFrameTrigger(parent, channels,
                    sampleFrequency);
            break;
        case 5:
            newFilter = createLowPassFilter(parent, channels);
            break;
        case 6:
            newFilter = createHighPassFilter(parent, channels);
            break;
        case 7:
            newFilter = createEnvelopeFilter(parent, channels,
                    sampleFrequency);
            break;
        case 8:
            newFilter = createLowPassButterworth(parent, channels,
                    sampleFrequency);
            break;
        case 9:
            newFilter = createHighPassButterFilter(parent, channels,
                    sampleFrequency);
            break;
        case 10:
            newFilter = createBandPassButterFilter(parent, channels,
                    sampleFrequency);
            break;
        case 11:
            newFilter = createBandStopButterFiltter(parent, channels,
                    sampleFrequency);
            break;
        case 12:
            newFilter = createTestFilter(parent, channels,
                    sampleFrequency);
            break;
        case 13:
            newFilter = createMultiFilter(parent, channels,
                    sampleFrequency);
            break;
        }
        return newFilter;
    }

    Filter* FilterFactory::createMovingAverageFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Duration (ms):"), filterTypeName[0].c_str(),
                    wxT("10"));
                //Chris' MMG SMA filter = 250
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::SMAFilter(longValue, channels, sampleFrequency);
    }

    Filter* FilterFactory::createMedianFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Duration (ms):"), filterTypeName[1].c_str(),
                    wxT("3"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::MedianFilter(longValue, channels, sampleFrequency);
    }

    Filter* FilterFactory::createBinaryTrigger(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue;
        double doubleValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Threshold:"), filterTypeName[2].c_str(),
                    wxT("0.5"));
                //Chris' MMG trigger filter = 73/255
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToDouble(&doubleValue)
                || doubleValue <= 0 || doubleValue > 1)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a number between 0 and 1."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get binary trigger duration.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                wxT("Duration (ms):"), filterTypeName[2].c_str(),
                wxT("10"));
                //Chris' MMG trigger filter = 461
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::BinaryTrigger(doubleValue, longValue, channels,
                sampleFrequency);
    }

    Filter* FilterFactory::createShortTermEnergyFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Duration (ms):"), filterTypeName[3].c_str(),
                    wxT("10"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::STEFilter(longValue, channels, sampleFrequency);
    }

    Filter* FilterFactory::createFrameTrigger(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue1, longValue2;
        double doubleValue1, doubleValue2;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Minimum Threshold:"), filterTypeName[4].c_str(),
                    wxT("0.5"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToDouble(&doubleValue1)
                || doubleValue1 <= 0 || doubleValue1 > 1)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a number between 0 and 1."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get frame trigger minimum duration.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Minimum Duration (ms):"), filterTypeName[4].c_str(),
                    wxT("1"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue1)
                || longValue1 <= 0 || longValue1 > 1000)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get frame trigger maximum value.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Maximum Threshold:"), filterTypeName[4].c_str(),
                    wxT("1.0"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToDouble(&doubleValue2)
                || doubleValue2 <= 0 || doubleValue2 > 1
                || doubleValue2 <= doubleValue1)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a number between 0 and 1"
                            ", greater than the minimum."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get frame trigger maximum duration.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Maximum Duration (ms):"), filterTypeName[4].c_str(),
                    wxT("10"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue2)
                || longValue2 <= 0 || longValue2 > 1000
                || longValue2 <= longValue1)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 1000."
                            ", greater than the minimum."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::FrameTrigger(doubleValue1, longValue1,
                doubleValue2, longValue2, channels, sampleFrequency);
    }

    Filter* FilterFactory::createLowPassFilter(wxWindow* parent,
            const unsigned int channels)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Cutoff frequency (Hz):"), filterTypeName[5].c_str(),
                    wxT("20"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::LowPassFilter((unsigned int)longValue, channels);
    }

    Filter* FilterFactory::createHighPassFilter(wxWindow* parent,
            const unsigned int channels)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Cutoff frequency (Hz):"), filterTypeName[6].c_str(),
                    wxT("20"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::HighPassFilter((unsigned int)longValue, channels);
    }

    Filter* FilterFactory::createEnvelopeFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Dutaion (ms):"), filterTypeName[7].c_str(),
                    wxT("64"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue)
                || longValue <= 0 || longValue > 512)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 513"
                            " (will be increased to next power of 2)."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::EnvelopeFilter(longValue, channels, sampleFrequency);
    }

    Filter* FilterFactory::createLowPassButterworth(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue1, longValue2;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Order:"), filterTypeName[8].c_str(),
                    wxT("2"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue1)
                || longValue1 <= 0 || longValue1 >= 10)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 10"
                            " (higher order filters are unstable)."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get low-pass Butterworth filter cutoff frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Cutoff frequency (Hz):"), filterTypeName[8].c_str(),
                    wxT("20"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue2)
                || longValue2 <= 0 || longValue2 >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::DiffFilter(longValue1, longValue2, false, channels,
                sampleFrequency);
    }

    Filter* FilterFactory::createHighPassButterFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue1, longValue2;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Order:"), filterTypeName[9].c_str(),
                    wxT("2"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue1)
                || longValue1 <= 0 || longValue1 >= 10)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 10"
                            " (higher order filters are unstable)."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get high-pass Butterworth filter cutoff frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Cutoff frequency (Hz):"), filterTypeName[9].c_str(),
                    wxT("20"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue2)
                || longValue2 <= 0 || longValue2 >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::DiffFilter(longValue1, longValue2, true, channels,
                sampleFrequency);
    }

    Filter* FilterFactory::createBandPassButterFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue1, longValue2, longValue3;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Order:"), filterTypeName[10].c_str(),
                    wxT("2"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue1)
                || longValue1 <= 0 || longValue1 >= 10)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 10"
                            " (higher order filters are unstable)."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get band-pass Butterworth filter low frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("Low frequency (Hz):"), filterTypeName[10].c_str(),
                    wxT("10"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue2)
                || longValue2 <= 0 || longValue2 >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get band-pass Butterworth filter high frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                    wxT("High frequency (Hz):"), filterTypeName[10].c_str(),
                    wxT("50"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue3)
                || longValue3 <= 0 || longValue3 >= 500
                || longValue3 <= longValue2)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"
                            ", greater than the low frequency."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::DiffFilter(longValue1, longValue2, longValue3, false,
                channels, sampleFrequency);
    }

    Filter* FilterFactory::createBandStopButterFiltter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        bool valid = false;
        long longValue1, longValue2, longValue3;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                wxT("Order:"), filterTypeName[11].c_str(),
                wxT("2"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue1)
                || longValue1 <= 0 || longValue1 >= 10)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 10"
                            " (higher order filters are unstable)."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get band-stop Butterworth filter low frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                wxT("Low frequency (Hz):"), filterTypeName[11].c_str(),
                wxT("10"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue2)
                || longValue2 <= 0 || longValue2 >= 500)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        // Get band-stop Butterworth filter high frequency.
        valid = false;
        while (!valid)
        {
            wxTextEntryDialog* entryDialog = new wxTextEntryDialog(parent,
                wxT("High frequency (Hz):"), filterTypeName[11].c_str(),
                wxT("50"));
            if (entryDialog->ShowModal() == wxID_CANCEL) return 0;
            if (!entryDialog->GetValue().ToLong(&longValue3)
                || longValue3 <= 0 || longValue3 >= 500
                || longValue3 <= longValue2)
            {
                wxMessageDialog* messageDialog = new wxMessageDialog(parent,
                        wxT("Must be a positive integer less than 500"
                            ", greater than the low frequency."),
                        wxT("Invalid Entry"));
                messageDialog->ShowModal();
            }
            else
            {
                valid = true;
            }
        }
        return new ma::DiffFilter(longValue1, longValue2, longValue3, true,
                channels, sampleFrequency);
    }

    Filter* FilterFactory::createTestFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        return new ma::TestFilter(channels, sampleFrequency);
    }

    Filter* FilterFactory::createMultiFilter(wxWindow* parent,
            const unsigned int channels, const unsigned int sampleFrequency)
    {
        MultiFilter* newMultiFilter = new ma::MultiFilter(channels,
                sampleFrequency);
        MultiFilterDialog multiFilterDialog(newMultiFilter, parent);
        multiFilterDialog.ShowModal();
        return newMultiFilter;
    }

    const unsigned int FilterFactory::filterTypeCount = 14;
    const wxString FilterFactory::filterTypeName[filterTypeCount] = {
        "Moving Average Filter",    // 0
        "Median Filter",            // 1
        "Binary Trigger",           // 2
        "Short-Term Energy Filter", // 3
        "Frame Trigger",            // 4
        "Low-Pass Filter",          // 5
        "High-Pass Filter",         // 6
        "Envelope Filter",          // 7
        "Low-Pass Butterworth",     // 8
        "High-Pass ButterFilter",   // 9
        "Band-Pass ButterFilter",   // 10
        "Band-Stop ButterFiltter",  // 11
        "TestFilter",               // 12
        "MultiFilter"               // 13
    };
}
