// Creates a binary output that is set to 1 if the input is above a threshold
// for a period.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef BINARYTRIGGER_H
#define BINARYTRIGGER_H

#include "filter.h"
#include "temporaldata.h"

namespace ma
{
    class BinaryTrigger : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param threshold         The threshold that needs to be exceeded.
         * @param duration          The duration in milliseconds needed above
         *                          threshold.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        BinaryTrigger(const double threshold = 0.5,
                const unsigned int duration = 1,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Temporal data structure constructor.
         *
         * @param threshold     The threshold that needs to be exceeded.
         * @param temporalData  The temporal data structure to copy.
         */
        BinaryTrigger(const double threshold, const TemporalData& temporalData);

        /**
         * Copy constructor.
         *
         * @param source    The binary trigger to copy.
         */
        BinaryTrigger(const BinaryTrigger& source);

        /**
         * Default destructor.
         */
        virtual ~BinaryTrigger();

        /**
         * Swap function.
         *
         * @param first  The first binary trigger to swap with.
         * @param second The second binary trigger to swap with.
         */
        friend void swap(BinaryTrigger& first, BinaryTrigger& second);

        /**
         * Assignment operator.
         * Returns a copy of the binary trigger.
         *
         * @param source The binary trigger to copy.
         * @return       A copy of the binary trigger.
         */
        BinaryTrigger& operator=(BinaryTrigger source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns the filter's threshold.
         *
         * @return The filter's threshold.
         */
        double getThreshold() const;

        /**
         * Returns a copy of the underlying temporal data structure.
         *
         * @return A copy of the underlying temporal data structure.
         */
        TemporalData getTemporalData() const;

    private:
        double getOuput(const Vector& historicData);

        TemporalData data;
        double threshold;
    };

    /**
     * Input stream operator.
     * Builds a binary trigger from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The binary trigger to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, BinaryTrigger& destination);

    /**
     * Output stream operator.
     * Appends the binary trigger to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The binary trigger to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const BinaryTrigger& source);
}

#endif // BINARYTRIGGER_H
