// (c) 2016 - 2017: Marcel Admiraal

#include "envelopefilter.h"
#include "fft.h"

#include <cmath>

namespace ma
{
    EnvelopeFilter::EnvelopeFilter(const unsigned int duration,
            const unsigned int channels, const unsigned int sampleFrequency) :
        length(getLogableLength(duration)),
        data(channels, sampleFrequency, length)
    {
    }

    EnvelopeFilter::EnvelopeFilter(const EnvelopeFilter& source) :
        length(source.length), data(source.data)
    {
    }

    EnvelopeFilter::EnvelopeFilter(const TemporalData& temporalData) :
        length(getLogableLength(temporalData.getDataLength())),
        data(temporalData.getChannels(), length,
                temporalData.getUpdatePeriod(), 0)
    {
    }

    EnvelopeFilter::~EnvelopeFilter()
    {
    }

    void swap(EnvelopeFilter& first, EnvelopeFilter& second)
    {
        std::swap(first.length, second.length);
        swap(first.data, second.data);
    }

    EnvelopeFilter& EnvelopeFilter::operator=(EnvelopeFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double EnvelopeFilter::filter(const double input,
            const unsigned int channel, const unsigned int milliseconds)
    {
        double dData[length];
        data.addData(input, channel, milliseconds);
        data.getData(dData, channel, false);
        instantaneousMagnitude(dData, length);
        return dData[length-1];
    }

    Filter* EnvelopeFilter::createCopy() const
    {
        EnvelopeFilter* aCopy = new EnvelopeFilter(*this);
        return aCopy;
    }

    unsigned int EnvelopeFilter::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int EnvelopeFilter::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int EnvelopeFilter::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str EnvelopeFilter::getDescription() const
    {
        Str result = "Envelope filter (Duration: " +
                toStr(data.getDataLength() * data.getUpdatePeriod()) + ")";
        return result;
    }

    TemporalData EnvelopeFilter::getTemporalData() const
    {
        return data;
    }

    void instantaneousMagnitude(double data[], const unsigned int length)
    {
        Complex cData[length];
        for (unsigned int i = 0; i < length; ++i) cData[i] = data[i];
        fft(cData, length);
        // Definition of the Fourier transform of the analytic function:
        // Double the positive frequencies and zero the negative frequencies.
        // Offset is kept the same.
        for (unsigned int i = 1; i < length/2; ++i) cData[i] *= 2;
        if (length/2 > 0)   // Make sure we don't zero the null filter.
        for (unsigned int i = length/2; i < length; ++i) cData[i] = 0;
        ifft(cData, length);
        for (unsigned int i = 0; i < length; ++i) data[i] = cData[i].abs();
    }

    unsigned int getLogableLength(const unsigned int duration)
    {
        double logDuration = log2(duration);
        unsigned int duration2x = 1 << (unsigned int)logDuration;
        if ((unsigned int)logDuration != logDuration)
            duration2x <<= 1;
        return duration2x;
    }

    std::istream& operator>>(std::istream& is, EnvelopeFilter& destination)
    {
        TemporalData temporalData;
        is >> temporalData;
        if (is.good())
        {
            destination = EnvelopeFilter(temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const EnvelopeFilter& source)
    {
        os << source.getTemporalData();
        return os;
    }
}
