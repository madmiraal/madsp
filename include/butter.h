// Functions for creating a Butterworth filter.
//
// (c) 2017: Marcel Admiraal

#include "vector.h"
#include "vectorcomplex.h"

namespace ma
{
    /**
     * Transform edges of a generic low-pass filter represented in splane
     * zero-pole-gain form to the required low or high pass filter. Filter
     * edges are specified in radians, from 0 to pi (the nyquist frequency).
     *
     * @param zero  The generic low-pass zeros.
     * @param pole  The generic low-pass poles.
     * @param gain  The generic low-pass gain.
     * @param Fc    The required cut-off nyquist frequency.
     * @param stop  Whether or not this is a high-pass filter.
     */
    void sftrans(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double Fc, const bool high = false);

    /**
     * Transform edges of a generic low-pass filter represented in splane
     * zero-pole-gain form to the required band or stop pass filter. Filter
     * edges are specified in radians, from 0 to pi (the nyquist frequency).
     *
     * @param zero  The generic low-pass zeros.
     * @param pole  The generic low-pass poles.
     * @param gain  The generic low-pass gain.
     * @param Fl    The required low nyquist frequency.
     * @param Fh    The required high nyquist frequency.
     * @param stop  Whether or not this is a high-pass filter.
     */
    void sftrans(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double Fl, const double Fh, const bool stop = false);

    /**
     * Transform an s-plane filter specification into a z-plane
     * specification. Filters are specified in zero-pole-gain form.
     *
     * @param zero  The filter zeros.
     * @param pole  The filter poles.
     * @param gain  The filter gain.
     * @param T     The inverse of the sampling frequency in the z plane.
     */
    void bilinear(VectorComplex& zero, VectorComplex& pole, double& gain,
            const double T);

    /**
     * Returns a vector of the coefficients of the polynomial whose roots are
     * the elements of the vector passed.
     *
     * @param root  The vector with the roots.
     * @return      The vector with the polynomial coefficients.
     */
    VectorComplex poly(const VectorComplex& root);

    /**
     * Generate the low or high-pass Butterworth filter parameters.
     *
     * @param order     The order of the Butterworth filter to generate.
     * @param frequency The cutoff frequency in Hz.
     * @param sample    The sample frequency in Hz.
     * @param a         The Butterworth output parameters.
     * @param b         The Butterworth input parameters.
     * @param high      Whether or not a high-pass filter is required.
     */
    void butter(const unsigned int order, const unsigned int frequency,
            const unsigned int sample,
            Vector& a, Vector& b, const bool high = false);

    /**
     * Generate the band-pass or stop Butterworth filter parameters.
     *
     * @param order     The order of the Butterworth filter to generate.
     * @param low       The low frequency in Hz.
     * @param high      The high frequency in Hz.
     * @param sample    The sample frequency in Hz.
     * @param a         The Butterworth output parameters.
     * @param b         The Butterworth input parameters.
     * @param stop      Whether or not a band-stop filter is required.
     */
    void butter(const unsigned int order, const unsigned int low,
            const unsigned int high, const unsigned int sample,
            Vector& a, Vector& b, const bool stop = false);

    /**
     * Generate the low or high-pass Butterworth filter zeros, poles and gain.
     *
     * @param order     The order of the Butterworth filter to generate.
     * @param frequency The cutoff frequency in Hz.
     * @param sample    The sample frequency in Hz.
     * @param zero      The zeros.
     * @param pole      The poles.
     * @param gain      The gain.
     * @param high      Whether or not a high-pass filter is required.
     */
    void butter(const unsigned int order, const unsigned int frequency,
            const unsigned int sample,
            VectorComplex& zero, VectorComplex& pole, double& gain,
            const bool high = false);

    /**
     * Generate the band-pass or stop Butterworth filter zeros, poles and gain.
     *
     * @param order     The order of the Butterworth filter to generate.
     * @param low       The low frequency in Hz.
     * @param high      The high frequency in Hz.
     * @param sample    The sample frequency in Hz.
     * @param zero      The zeros.
     * @param pole      The poles.
     * @param gain      The gain.
     * @param stop      Whether or not a band-stop filter is required.
     */
    void butter(const unsigned int order, const unsigned int low,
            const unsigned int high, const unsigned int sample,
            VectorComplex& zero, VectorComplex& pole, double& gain,
            const bool stop = false);
}
