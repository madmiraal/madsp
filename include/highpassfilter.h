// A simple high-pass filter.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef HIGHPASSFILTER_H
#define HIGHPASSFILTER_H

#include "filter.h"
#include "vector.h"

#include <chrono>

namespace ma
{
    class HighPassFilter : public Filter
    {
    public:
        /**
         * Cutoff constructor.
         *
         * @param cutoff            The cutoff frequency in Hz. Default: 1
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        HighPassFilter(const unsigned int cutoff = 1,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * RC constructor.
         *
         * @param RC                The filter's RC value.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        HighPassFilter(const double RC, const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Copy constructor.
         *
         * @param source    The high pass filter to copy.
         */
        HighPassFilter(const HighPassFilter& source);

        /**
         * Default destructor.
         */
        virtual ~HighPassFilter();

        /**
         * Swap function.
         *
         * @param first  The first high pass filter to swap with.
         * @param second The second high pass filter to swap with.
         */
        friend void swap(HighPassFilter& first, HighPassFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the high pass filter.
         *
         * @param source The high pass filter to copy.
         * @return       A copy of the high pass filter.
         */
        HighPassFilter& operator=(HighPassFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the filter's channels.
         *
         * @return The filter's channels.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns the filter's RC value.
         *
         * @return The filter's RC value.
         */
        double getRC() const;

    private:
        void initialise();

        double RC;
        double* previousInput;
        double* filteredValue;
        unsigned int cutoff;
        unsigned int channels;
        unsigned int sampleFrequency;
        std::chrono::milliseconds previousTime;
    };

    /**
     * Input stream operator.
     * Builds a high pass filter from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The high pass filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, HighPassFilter& destination);

    /**
     * Output stream operator.
     * Appends the high pass filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The high pass filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const HighPassFilter& source);
}

#endif // HIGHPASSFILTER_H
