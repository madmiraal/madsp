// A filter that combines multiple filters.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef MULTIFILTER_H
#define MULTIFILTER_H

#include "filter.h"
#include "list.h"

#include <mutex>

namespace ma
{
    class MultiFilter : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        MultiFilter(const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Copy constructor.
         *
         * @param source    The multi-filter to copy.
         */
        MultiFilter(const MultiFilter& source);

        /**
         * Default destructor.
         */
        virtual ~MultiFilter();

        /**
         * Swap function.
         *
         * @param first  The first multi-filter to swap with.
         * @param second The second multi-filter to swap with.
         */
        friend void swap(MultiFilter& first, MultiFilter& second);

        /**
         * Filters the data in real-time.
         *
         * @param input     The new data.
         * @param channel   The channel to add the data to. Default: 0
         * @return          The filtered data.
         */
        MultiFilter& operator=(MultiFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Populates the array with the filters in this multi-filter's
         * descriptions.
         *
         * @param strArray  The array to populate.
         */
        void descriptions(Str strArray[]) const;

        /**
         * Add a filter to the end of the existing filters. The new filter must
         * have the same number of channels.
         * The multi-filter will make a copy of the filter.
         *
         * @param newFilter A pointer to the filter to add.
         * @return          Whether or not the filter was added successfully.
         */
        bool addFilter(Filter* newFilter);

        /**
         * Clears all filters that were previously added.
         */
        void clearFilters();

        /**
         * Returns the number of filters used by this multi-filter.
         *
         * @return The number of filters used by this multi-filter.
         */
        unsigned int filterCount() const;

        /**
         * Returns a pointer to a copy of the required filter.
         *
         * @return A pointer to a copy of the required filter.
         */
        Filter* getFilter(unsigned int index) const;

    private:
        List<Filter*> filterList;
        unsigned int channels;
        unsigned int sampleFrequency;
        std::mutex dataLock;
    };

    /**
     * Input stream operator.
     * Builds a multi-filter from an input stream and returns the input stream.
     *
     * @param is            The input stream.
     * @param destination   The multi-filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, MultiFilter& destination);

    /**
     * Output stream operator.
     * Appends the multi-filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The multi-filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const MultiFilter& source);

    enum class FilterType
    {
        BinaryTrigger,  // 0
        DiffFilter,     // 1
        EnvelopeFilter, // 2
        FrameTrigger,   // 3
        HighPassFilter, // 4
        LowPassFilter,  // 5
        MultiFilter,    // 6
        MedianFilter,   // 7
        SMAFilter,      // 8
        STEFilter,      // 9
        TestFilter      // 10
    };
}

#endif // MULTIFILTER_H
