// Creates the dialogs to prompt for the parameters to create a required
// filter.
//
// (c) 2017: Marcel Admiraal

#ifndef FILTERFACTORY_H
#define FILTERFACTORY_H

#include <wx/string.h>
#include <wx/window.h>

#include "filter.h"

namespace ma
{
    class FilterFactory
    {
    public:
        /**
         * Returns the number of types of filters.
         *
         * @return The number of types of filters.
         */
        static const unsigned int getFilterTypeCount();

        /**
         * Returns an array of wxStrings containing the filter type names.
         *
         * @return An array of wxStrings containing the filter type names.
         */
        static const wxString* getFilterTypeNames();

        /**
         * Creates the dialogs to prompt for the parameters for the required
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param index             The index of the filter type.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createFilter(wxWindow* parent, const unsigned int index,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a moving average
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createMovingAverageFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a median
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createMedianFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);
        /**
         * Creates the dialogs to prompt for the parameters for a binary trigger
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createBinaryTrigger(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a short-term
         * energy filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createShortTermEnergyFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a frame trigger
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createFrameTrigger(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a low pass
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         */
        static Filter* createLowPassFilter(wxWindow* parent,
                const unsigned int channels = 1);

        /**
         * Creates the dialogs to prompt for the parameters for a high pass
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         */
        static Filter* createHighPassFilter(wxWindow* parent,
                const unsigned int channels = 1);

        /**
         * Creates the dialogs to prompt for the parameters for a envelope
         * filter and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createEnvelopeFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a low-pass
         * Betterworth filter and then creates and returns a pointer to the
         * filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createLowPassButterworth(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a high-pass
         * Butterworth filter and then creates and returns a pointer to the
         * filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createHighPassButterFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a band-pass
         * Butterworth filter and then creates and returns a pointer to the
         * filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createBandPassButterFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a band-stop
         * Butterworth filter and then creates and returns a pointer to the
         * filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createBandStopButterFiltter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a test filter
         * and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createTestFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Creates the dialogs to prompt for the parameters for a multi filter
         * and then creates and returns a pointer to the filter.
         *
         * @param parent            A pointer to the parent window.
         * @param channels          The number of channels. Default: 1
         * @param sampleFrequency   The expected sample frequency.
         *                          Default 1000 Hz.
         */
        static Filter* createMultiFilter(wxWindow* parent,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

    private:
        static const unsigned int filterTypeCount;
        static const wxString filterTypeName[];
    };
}

#endif // FILTERFACTORY_H
