// A simple low-pass filter.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef LOWPASSFILTER_H
#define LOWPASSFILTER_H

#include "filter.h"
#include "vector.h"

#include <chrono>

namespace ma
{
    class LowPassFilter : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param cutoff            The cutoff frequency in Hz. Default: 1000
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        LowPassFilter(const unsigned int cutoff = 1000,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * RC constructor.
         *
         * @param RC                The filter's RC value.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        LowPassFilter(const double RC, const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Copy constructor.
         *
         * @param source    The low pass filter to copy.
         */
        LowPassFilter(const LowPassFilter& source);

        /**
         * Default destructor.
         */
        virtual ~LowPassFilter();

        /**
         * Swap function.
         *
         * @param first  The first low pass filter to swap with.
         * @param second The second low pass filter to swap with.
         */
        friend void swap(LowPassFilter& first, LowPassFilter& second);

        /**
         * Assignment operator.
         * Returns a copy of the low pass filter.
         *
         * @param source The low pass filter to copy.
         * @return       A copy of the low pass filter.
         */
        LowPassFilter& operator=(LowPassFilter source);

        /**
         * Filters data in real-time.
         *
         * @param input     The new data value.
         * @param channel   The channel to add the data to. Default: 0
         * @return          The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the filter's channels.
         *
         * @return The filter's channels.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns the filter's RC value.
         *
         * @return The filter's RC value.
         */
        double getRC() const;

    private:
        void initialise();

        double RC;
        double* filteredValue;
        unsigned int cutoff;
        unsigned int channels;
        unsigned int sampleFrequency;
        std::chrono::milliseconds previousTime;
    };

    /**
     * Input stream operator.
     * Builds a low pass filter from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The low pass filter to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, LowPassFilter& destination);

    /**
     * Output stream operator.
     * Appends the low pass filter to the output stream and returns the output
     * stream.
     *
     * @param os     The output stream.
     * @param source The low pass filter to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const LowPassFilter& source);
}

#endif // LOWPASSFILTER_H
