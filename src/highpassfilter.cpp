// (c) 2016 - 2017: Marcel Admiraal

#include "highpassfilter.h"

#include <cmath>

namespace ma
{
    HighPassFilter::HighPassFilter(const unsigned int cutoff,
            const unsigned int channels, unsigned int sampleFrequency) :
        RC(1.0 / (2 * M_PI * cutoff)), cutoff(cutoff), channels(channels),
        sampleFrequency(sampleFrequency)
    {
        initialise();
    }

    HighPassFilter::HighPassFilter(const double RC, const unsigned int channels,
            const unsigned int sampleFrequency) :
        RC(RC), cutoff(1.0 / (2 * M_PI * RC) + .5), channels(channels),
        sampleFrequency(sampleFrequency)
    {
        initialise();
    }

    HighPassFilter::HighPassFilter(const HighPassFilter& source) :
        RC(source.RC), cutoff(source.cutoff), channels(source.channels),
        sampleFrequency(source.sampleFrequency)
    {
        initialise();
    }

    HighPassFilter::~HighPassFilter()
    {
        delete[] previousInput;
        delete[] filteredValue;
    }

    void swap(HighPassFilter& first, HighPassFilter& second)
    {
        std::swap(first.RC, second.RC);
        std::swap(first.previousInput, second.previousInput);
        std::swap(first.filteredValue, second.filteredValue);
        std::swap(first.cutoff, second.cutoff);
        std::swap(first.channels, second.channels);
        std::swap(first.sampleFrequency, second.sampleFrequency);
        std::swap(first.previousTime, second.previousTime);
    }

    HighPassFilter& HighPassFilter::operator=(HighPassFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double HighPassFilter::filter(const double input,
            const unsigned int channel, const unsigned int milliseconds)
    {
        // Calculate time since last input.
        std::chrono::milliseconds now =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
        double dt = milliseconds / 1000.;
        if (dt == 0) dt = (now - previousTime).count() / 1000.;
        previousTime = now;

        // Filter data.
        double alpha = RC / (RC + dt);
        filteredValue[channel] = alpha * (filteredValue[channel] + input -
                previousInput[channel]);
        previousInput[channel] = input;
        return filteredValue[channel];
    }

    Filter* HighPassFilter::createCopy() const
    {
        HighPassFilter* aCopy = new HighPassFilter(*this);
        return aCopy;
    }

    unsigned int HighPassFilter::getChannels() const
    {
        return channels;
    }

    unsigned int HighPassFilter::getSampleFrequency() const
    {
        return sampleFrequency;
    }

    unsigned int HighPassFilter::getSamplePeriod() const
    {
        return 1000 / sampleFrequency;
    }

    Str HighPassFilter::getDescription() const
    {
        Str result = "High-pass filter (Cutoff: " + toStr(cutoff) + " Hz)";
        return result;
    }

    double HighPassFilter::getRC() const
    {
        return RC;
    }

    void HighPassFilter::initialise()
    {
        previousInput = new double[channels]();
        filteredValue = new double[channels]();
        previousTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::steady_clock::now().time_since_epoch());
    }

    std::istream& operator>>(std::istream& is, HighPassFilter& destination)
    {
        unsigned int channels;
        double RC;
        is >> channels;
        is >> RC;
        if (is.good())
        {
            destination = HighPassFilter(RC, channels);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const HighPassFilter& source)
    {
        os << source.getChannels() << '\t';
        os << source.getRC();
        return os;
    }
}
