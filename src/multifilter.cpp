// (c) 2016 - 2017: Marcel Admiraal

#include "multifilter.h"

#include "binarytrigger.h"
#include "difffilter.h"
#include "envelopefilter.h"
#include "frametrigger.h"
#include "highpassfilter.h"
#include "lowpassfilter.h"
#include "multifilter.h"
#include "medianfilter.h"
#include "smafilter.h"
#include "stefilter.h"
#include "testfilter.h"

#include "iterator.h"
#include "strstream.h"

namespace ma
{
    MultiFilter::MultiFilter(const unsigned int channels,
            const unsigned int sampleFrequency) :
        channels(channels), sampleFrequency(sampleFrequency)
    {
    }

    MultiFilter::MultiFilter(const MultiFilter& source) :
        channels(source.channels), sampleFrequency(source.sampleFrequency)
    {
        for (Iterator<Filter*> iter = source.filterList.first();
                iter.valid(); ++iter)
        {
            filterList.append((*iter)->createCopy());
        }
    }

    MultiFilter::~MultiFilter()
    {
        clearFilters();
    }

    void swap(MultiFilter& first, MultiFilter& second)
    {
        std::swap(first.filterList, second.filterList);
        std::swap(first.channels, second.channels);
        std::swap(first.sampleFrequency, second.sampleFrequency);
    }

    MultiFilter& MultiFilter::operator=(MultiFilter source)
    {
        std::lock_guard<std::mutex> lock(dataLock);
        swap(*this, source);
        return *this;
    }

    double MultiFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        double result = input;
        std::lock_guard<std::mutex> lock(dataLock);
        for (Iterator<Filter*> iter = filterList.first(); iter.valid(); ++iter)
        {
            result = (*iter)->filter(result, channel, milliseconds);
        }
        return result;
    }

    Filter* MultiFilter::createCopy() const
    {
        MultiFilter* aCopy = new MultiFilter(*this);
        return aCopy;
    }

    unsigned int MultiFilter::getChannels() const
    {
        return channels;
    }

    unsigned int MultiFilter::getSampleFrequency() const
    {
        return sampleFrequency;
    }

    unsigned int MultiFilter::getSamplePeriod() const
    {
        return 1000 / sampleFrequency;
    }

    Str MultiFilter::getDescription() const
    {
        StrStream strStream;
        unsigned int filters = filterCount();
        strStream << "Multi-filter with " + toStr((long)filters);
        if (filters == 0)      strStream << " filters";
        else if (filters == 1) strStream << " filter:";
        else                   strStream << " filters:";
        for (Iterator<Filter*> iter = filterList.first(); iter.valid(); ++iter)
        {
            strStream << "\n" << (*iter)->getDescription();
        }
        return strStream.toStr();
    }

    void MultiFilter::descriptions(Str result[]) const
    {
        unsigned int index = 0;
        for (Iterator<Filter*> iter = filterList.first(); iter.valid(); ++iter)
        {
            result[index] = (*iter)->getDescription();
            ++index;
        }
    }

    bool MultiFilter::addFilter(Filter* newFilter)
    {
        if (newFilter->getChannels() != channels) return false;
        std::lock_guard<std::mutex> lock(dataLock);
        filterList.append(newFilter->createCopy());
        return true;
    }

    void MultiFilter::clearFilters()
    {
        std::lock_guard<std::mutex> lock(dataLock);
        for (Iterator<Filter*> iter = filterList.first(); iter.valid(); ++iter)
        {
            delete *iter;
        }
        filterList.clear();
    }

    unsigned int MultiFilter::filterCount() const
    {
        return filterList.getSize();
    }

    Filter* MultiFilter::getFilter(unsigned int index) const
    {
        if (index >= filterList.getSize()) return 0;
        return filterList.get(index)->createCopy();
    }

    std::istream& operator>>(std::istream& is, MultiFilter& destination)
    {
        BinaryTrigger binaryTrigger;
        DiffFilter diffFilter;
        EnvelopeFilter envelopeFilter;
        FrameTrigger frameTrigger;
        HighPassFilter highPassFilter;
        LowPassFilter lowPassFilter;
        MultiFilter multiFilter;
        MedianFilter medianFilter;
        SMAFilter smaFilter;
        STEFilter steFilter;
        TestFilter testFilter;

        unsigned int filter = 0;
        unsigned int channels;
        unsigned int filters;
        is >> channels;
        is >> filters;
        MultiFilter result(channels);
        while (is.good() && filter < filters)
        {
            Filter* thisFilter = 0;
            unsigned int type = -1;
            is >> type;
            switch ((FilterType)type)
            {
            case FilterType::BinaryTrigger:
                is >> binaryTrigger;
                thisFilter = &binaryTrigger;
                break;
            case FilterType::DiffFilter:
                is >> diffFilter;
                thisFilter = &diffFilter;
                break;
            case FilterType::EnvelopeFilter:
                is >> envelopeFilter;
                thisFilter = &envelopeFilter;
                break;
            case FilterType::FrameTrigger:
                is >> frameTrigger;
                thisFilter = &frameTrigger;
                break;
            case FilterType::HighPassFilter:
                is >> highPassFilter;
                thisFilter = &highPassFilter;
                break;
            case FilterType::LowPassFilter:
                is >> lowPassFilter;
                thisFilter = &lowPassFilter;
                break;
            case FilterType::MultiFilter:
                is >> multiFilter;
                thisFilter = &multiFilter;
                break;
            case FilterType::MedianFilter:
                is >> medianFilter;
                thisFilter = &medianFilter;
                break;
            case FilterType::SMAFilter:
                is >> smaFilter;
                thisFilter = &smaFilter;
                break;
            case FilterType::STEFilter:
                is >> steFilter;
                thisFilter = &steFilter;
                break;
            case FilterType::TestFilter:
                is >> testFilter;
                thisFilter = &testFilter;
                break;
            default:
                break;
            }
            if (is.good() && thisFilter)
            {
                result.addFilter(thisFilter);
                ++filter;
            }
        }
        if (is.good() && result.filterCount() == filters)
        {
            swap(destination, result);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const MultiFilter& source)
    {
        unsigned int channels = source.getChannels();
        unsigned int filters = source.filterCount();
        os << channels << '\t';
        os << filters;
        for (unsigned int filter = 0; filter < filters; ++filter)
        {
            Filter* thisFilter = source.getFilter(filter);
            BinaryTrigger* binaryTrigger =
                    dynamic_cast<BinaryTrigger*>(thisFilter);
            DiffFilter* diffFilter =
                    dynamic_cast<DiffFilter*>(thisFilter);
            EnvelopeFilter* envelopeFilter =
                    dynamic_cast<EnvelopeFilter*>(thisFilter);
            FrameTrigger* frameTrigger =
                    dynamic_cast<FrameTrigger*>(thisFilter);
            HighPassFilter* highPassFilter =
                    dynamic_cast<HighPassFilter*>(thisFilter);
            LowPassFilter* lowPassFilter =
                    dynamic_cast<LowPassFilter*>(thisFilter);
            MultiFilter* multiFilter =
                    dynamic_cast<MultiFilter*>(thisFilter);
            MedianFilter* medianFilter =
                    dynamic_cast<MedianFilter*>(thisFilter);
            SMAFilter* smaFilter =
                    dynamic_cast<SMAFilter*>(thisFilter);
            STEFilter* steFilter =
                    dynamic_cast<STEFilter*>(thisFilter);
            TestFilter* testFilter =
                    dynamic_cast<TestFilter*>(thisFilter);
            if (binaryTrigger)
            {
                os << std::endl << (unsigned int)FilterType::BinaryTrigger << '\t';
                os << *binaryTrigger;
            }
            if (diffFilter)
            {
                os << std::endl << (unsigned int)FilterType::DiffFilter << '\t';
                os << *diffFilter;
            }
            if (envelopeFilter)
            {
                os << std::endl << (unsigned int)FilterType::EnvelopeFilter << '\t';
                os << *envelopeFilter;
            }
            if (frameTrigger)
            {
                os << std::endl << (unsigned int)FilterType::FrameTrigger << '\t';
                os << *frameTrigger;
            }
            if (highPassFilter)
            {
                os << std::endl << (unsigned int)FilterType::HighPassFilter << '\t';
                os << *highPassFilter;
            }
            if (lowPassFilter)
            {
                os << std::endl << (unsigned int)FilterType::LowPassFilter << '\t';
                os << *lowPassFilter;
            }
            if (multiFilter)
            {
                os << std::endl << (unsigned int)FilterType::MultiFilter << '\t';
                os << *multiFilter;
            }
            if (medianFilter)
            {
                os << std::endl << (unsigned int)FilterType::MedianFilter << '\t';
                os << *medianFilter;
            }
            if (smaFilter)
            {
                os << std::endl << (unsigned int)FilterType::SMAFilter << '\t';
                os << *smaFilter;
            }
            if (steFilter)
            {
                os << std::endl << (unsigned int)FilterType::STEFilter << '\t';
                os << *steFilter;
            }
            if (testFilter)
            {
                os << std::endl << (unsigned int)FilterType::TestFilter << '\t';
                os << *testFilter;
            }
        }
        return os;
    }
}
