# MADSP #

Cross-platform C++ code for real-time Digital Signal Processing:

## Filters ##
* ma::Filter: The base abstract class that all filters are derived from.
* ma::MultiFilter: Combines multiple filters into a single filter.
* ma::SMAFilter: A simple moving average filter that averages the values for a period.
* ma::BinaryTrigger: Sets the output to 1 if the input is above a threshold for a period.
* ma::FrameTrigger: Sets the output to 1 if the input is above a threshold minimum threshold below a maximum threshold, for a minimum period, but not longer than a maximum period.
* ma::LowPassFilter: A simple low-pass filter.
* ma::HighPassFilter: A simple high-pass filter.
* ma::EnvelopeFilter: An envelope filter using the magnitude of the analytic signal to obtain the instantaneous amplitude.
* ma::DiffFilter: Direct Form II implementation of the standard difference equation:
Includes constructors for creating any order low, high, band-pass or band-stop Butterworth filters.

## Creating Filters ##
Filters can be created directly, using ma::FilterFactory or through ma::MultifilterPanel or ma::MultifilterDialog.

## Dependencies ##
This code depends on:
* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
