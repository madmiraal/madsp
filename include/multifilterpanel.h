// A panel and dialog for managing a multi-filter.
//
// (c) 2017: Marcel Admiraal

#ifndef MULTIFILTERPANEL_H
#define MULTIFILTERPANEL_H

#include <wx/panel.h>
#include <wx/dialog.h>
#include <wx/listctrl.h>

#include "multifilter.h"

namespace ma
{
    class MultiFilterPanel : public wxPanel
    {
    public:
        /**
         * Constructor.
         *
         * @param parent        Pointer to the parent window.
         * @param multiFilter   Pointer to the underlying multi-filter.
         */
        MultiFilterPanel(wxWindow* parent, MultiFilter* multiFilter);

        /**
         * Default destructor.
         */
        virtual ~MultiFilterPanel();

        /**
         * Set the underlying multi-filter.
         */
        void setMultiFilter(MultiFilter* multiFilter);

    private:
        void populateFilterList();

        void addFilter(wxCommandEvent& event);
        void clearFilters(wxCommandEvent& event);
        void loadFilters(wxCommandEvent& event);
        void saveFilters(wxCommandEvent& event);

        wxListCtrl* filterList;
        MultiFilter* multiFilter;
    };

    class MultiFilterDialog : public wxDialog
    {
    public:
        /**
         * Constructor.
         *
         * @param multiFilter   The multi-filter to edit.
         * @param parent        Pointer to the parent window.
         * @param id            The frame's id.
         * @param title         The text displayed at the top of the frame.
         * @param pos           A wxPoint specifying the location of the window.
         * @param size          A wxSize specifying the size of the window.
         * @param style         The combination of wxstyle codes.
         * @param name          A name for the window.
         */
        MultiFilterDialog(MultiFilter* multiFilter,
            wxWindow* parent, wxWindowID id = wxID_ANY,
            const wxString& title = wxT("Multi-Filter Configuration"),
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = wxDEFAULT_DIALOG_STYLE,
            const wxString& name = wxDialogNameStr);
    };
}

#endif // MULTIFILTERPANEL_H
