// double x = 0.9969 * (current - prev) + 0.9937 * xprevious;
// double z = 0.07296 * (xprevious + x) + 0.8541 * zpreviuos;
// xprevious = x;
// zprevious = z;
// return z;
//
// (c) 2016 - 2017: Marcel Admiraal

#include "testfilter.h"

namespace ma
{
    TestFilter::TestFilter(const unsigned int channels,
            const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, 2)
    {
        initialise(channels);
    }

    TestFilter::TestFilter(const TemporalData& temporalData) :
        data(temporalData)
    {
        initialise(data.getChannels());
    }

    TestFilter::TestFilter(const TestFilter& source) :
        data(source.data)
    {
        initialise(source.getChannels());
    }

    TestFilter::~TestFilter()
    {
        delete[] x;
        delete[] z;
    }

    void swap(TestFilter& first, TestFilter& second)
    {
        std::swap(first.data, second.data);
        std::swap(first.x, second.x);
        std::swap(first.z, second.z);
    }

    TestFilter& TestFilter::operator=(TestFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double TestFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        unsigned int updates = data.addData(input, channel, milliseconds);
        double history[2];
        data.getData(history, channel, 0, false);
        for (unsigned int update = 0; update < updates; ++update)
        {
            double delta = (history[1] - history[0]) / updates;
            double newX = 0.9969 * delta + 0.9937 * x[channel];
            z[channel] = 0.07296 * (x[channel] + newX) + 0.8541 * z[channel];
            x[channel] = newX;
        }
        return z[channel];
    }

    Filter* TestFilter::createCopy() const
    {
        TestFilter* aCopy = new TestFilter(*this);
        return aCopy;
    }

    unsigned int TestFilter::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int TestFilter::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int TestFilter::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str TestFilter::getDescription() const
    {
        Str result = "Test filter";
        return result;
    }

    TemporalData TestFilter::getTemporalData() const
    {
        return data;
    }

    void TestFilter::initialise(const unsigned int channels)
    {
        x = new double[channels]();
        z = new double[channels]();
    }

    std::istream& operator>>(std::istream& is, TestFilter& destination)
    {
        TemporalData temporalData;
        is >> temporalData;
        if (is.good())
        {
            destination = TestFilter(temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const TestFilter& source)
    {
        os << source.getTemporalData();
        return os;
    }
}
