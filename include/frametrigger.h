// Sets the output to 1 if the input is above a threshold minimum threshold
// below a maximum threshold, for a minimum period, but not longer than a
// maximum period.
//
// (c) 2016 - 2017: Marcel Admiraal

#ifndef FRAMETRIGGER_H
#define FRAMETRIGGER_H

#include "filter.h"
#include "temporaldata.h"

namespace ma
{
    class FrameTrigger : public Filter
    {
    public:
        /**
         * Constructor.
         *
         * @param minValue          The threshold that needs to be exceeded.
         * @param minTime           The minimum duration in milliseconds
         *                          required.
         * @param maxValue          The cap that needs to be kept below.
         * @param maxTime           The maximum duration in milliseconds allowed.
         * @param channels          The number of data channels. Default: 1
         * @param sampleFrequency   The expected sample frequency. Default: 1000Hz
         */
        FrameTrigger(const double minValue = 0.5, const unsigned int minTime = 1,
                const double maxValue = 1, const unsigned int maxTime = 10,
                const unsigned int channels = 1,
                const unsigned int sampleFrequency = 1000);

        /**
         * Constructor.
         *
         * @param minValue      The threshold that needs to be exceeded.
         * @param minTime       The minimum duration in milliseconds required.
         * @param maxValue      The cap that needs to be kept below.
         * @param maxTime       The maximum duration in milliseconds allowed.
         * @param temporalData  The temporal data structure to copy.
         */
        FrameTrigger(const double minValue, const unsigned int minTime,
                const double maxValue, const unsigned int maxTime,
                const TemporalData& data);

        /**
         * Copy constructor.
         *
         * @param source    The frame trigger to copy.
         */
        FrameTrigger(const FrameTrigger& source);

        /**
         * Default destructor.
         */
        virtual ~FrameTrigger();

        /**
         * Swap function.
         *
         * @param first  The first frame trigger to swap with.
         * @param second The second frame trigger to swap with.
         */
        friend void swap(FrameTrigger& first, FrameTrigger& second);

        /**
         * Assignment operator.
         * Returns a copy of the frame trigger.
         *
         * @param source The frame trigger to copy.
         * @return       A copy of the frame trigger.
         */
        FrameTrigger& operator=(FrameTrigger source);

        /**
         * Filters data in real-time.
         *
         * @param input         The new data value.
         * @param channel       The channel to add the data to. Default: 0
         * @param milliSeconds  If required, override the number of milliseconds
         *                      that have passed. Default: 0 = real-time.
         * @return              The filtered data.
         */
        virtual double filter(const double input,
                const unsigned int channel = 0,
                const unsigned int milliseconds = 0);

        /**
         * Returns a pointer to a copy of this filter.
         * The caller takes ownership of the filter and must delete it.
         *
         * @return A pointer to a copy of this filter.
         */
        virtual Filter* createCopy() const;

        /**
         * Returns the number of channels the filter has.
         *
         * @return The number of channels the filter has.
         */
        virtual unsigned int getChannels() const;

        /**
         * Returns the filter's expected sample frequency in Hz.
         *
         * @return The filter's expected sample frequency in Hz.
         */
        virtual unsigned int getSampleFrequency() const;

        /**
         * Returns the filter's expected sample period in ms.
         *
         * @return The filter's expected sample period in ms.
         */
        virtual unsigned int getSamplePeriod() const;

        /**
         * Returns a description of this filter.
         *
         * @return A description of this filter.
         */
        virtual Str getDescription() const;

        /**
         * Returns a copy of the underlying temporal data structure.
         *
         * @return A copy of the underlying temporal data structure.
         */
        TemporalData getTemporalData() const;

        /**
         * Returns the filter's minimum time.
         *
         * @return The filter's minimum time.
         */
        unsigned int getMinTime() const;

        /**
         * Returns the filter's minimum value.
         *
         * @return The filter's minimum value.
         */
        double getMinValue() const;

        /**
         * Returns the filter's maximum time.
         *
         * @return The filter's maxium time.
         */
        unsigned int getMaxTime() const;

        /**
         * Returns the filter's maximum value.
         *
         * @return The filter's maximum value.
         */
        double getMaxValue() const;

    private:
        bool testFrame(const Vector& historicData);

        TemporalData data;
        double minValue;
        unsigned int minTime;
        double maxValue;
        unsigned int minIndex;
    };

    /**
     * Input stream operator.
     * Builds a frame trigger from an input stream and returns the input
     * stream.
     *
     * @param is            The input stream.
     * @param destination   The frame trigger to build.
     * @return              The input stream.
     */
    std::istream& operator>>(std::istream& is, FrameTrigger& destination);

    /**
     * Output stream operator.
     * Appends the frame trigger to the output stream and returns the
     * output stream.
     *
     * @param os     The output stream.
     * @param source The frame trigger to stream.
     * @return       The output stream.
     */
    std::ostream& operator<<(std::ostream& os, const FrameTrigger& source);
}

#endif // FRAMETRIGGER_H
