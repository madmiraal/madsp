// (c) 2016 - 2017: Marcel Admiraal

#include "binarytrigger.h"

#include "strstream.h"

namespace ma
{
    BinaryTrigger::BinaryTrigger(const double threshold,
            const unsigned int duration, const unsigned int channels,
            const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, duration), threshold(threshold)
    {
    }

    BinaryTrigger::BinaryTrigger(const double threshold,
        const TemporalData& temporalData) :
        data(temporalData), threshold(threshold)
    {
    }

    BinaryTrigger::BinaryTrigger(const BinaryTrigger& source) :
        data(source.data), threshold(source.threshold)
    {
    }

    BinaryTrigger::~BinaryTrigger()
    {
    }

    void swap(BinaryTrigger& first, BinaryTrigger& second)
    {
        swap(first.data, second.data);
        std::swap(first.threshold, second.threshold);
    }

    BinaryTrigger& BinaryTrigger::operator=(BinaryTrigger source)
    {
        swap(*this, source);
        return *this;
    }

    double BinaryTrigger::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        data.addData(input, channel, milliseconds);
        return getOuput(data.getData(channel, false));
    }

    Filter* BinaryTrigger::createCopy() const
    {
        BinaryTrigger* aCopy = new BinaryTrigger(*this);
        return aCopy;
    }

    unsigned int BinaryTrigger::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int BinaryTrigger::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int BinaryTrigger::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str BinaryTrigger::getDescription() const
    {
        StrStream strStream;
        strStream << "Binary Trigger (At: " << threshold;
        strStream << " for: " << data.getDataLength() * data.getUpdatePeriod();
        strStream << " ms)";
        return strStream.toStr();
    }

    double BinaryTrigger::getThreshold() const
    {
        return threshold;
    }

    TemporalData BinaryTrigger::getTemporalData() const
    {
        return data;
    }

    double BinaryTrigger::getOuput(const Vector& historicData)
    {
        return historicData.min() > threshold ? 1 : 0;
    }

    std::istream& operator>>(std::istream& is, BinaryTrigger& destination)
    {
        TemporalData temporalData;
        double threshold;
        is >> temporalData;
        is >> threshold;
        if (is.good())
        {
            destination = BinaryTrigger(threshold, temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const BinaryTrigger& source)
    {
        os << source.getTemporalData() << '\t';
        os << source.getThreshold();
        return os;
    }
}
