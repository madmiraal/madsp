// (c) 2016 - 2017: Marcel Admiraal

#include "filter.h"

namespace ma
{
    Vector Filter::filterAll(const Vector& newInput,
            const unsigned int milliseconds)
    {
        unsigned int channels = getChannels();
        Vector newOutput(channels);
        for (unsigned int channel = 0; channel < channels; ++channel)
        {
            newOutput[channel] = filter(newInput[channel], channel,
                    milliseconds);
        }
        return newOutput;
    }

    Vector Filter::filterStatic(const Vector& input) const
    {
        Filter* staticFilter = createCopy();
        unsigned int dataLength = input.size();
        unsigned int samplePeriod = staticFilter->getSamplePeriod();
        Vector output(dataLength);
        for (unsigned int index = 0; index < dataLength; ++index)
        {
            output[index] = staticFilter->filter(input[index], 0, samplePeriod);
        }
        delete staticFilter;
        return output;
    }

    Vector filterStatic(const Filter* filter, const Vector& input)
    {
        return filter->filterStatic(input);
    }
}
