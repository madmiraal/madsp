// (c) 2016 - 2017: Marcel Admiraal

#include "medianfilter.h"

#include "algorithms.h"

namespace ma
{
    MedianFilter::MedianFilter(const unsigned int duration,
            const unsigned int channels, const unsigned int sampleFrequency) :
        data(channels, sampleFrequency, duration)
    {
    }

    MedianFilter::MedianFilter(const TemporalData& temporalData) :
        data(temporalData)
    {
    }

    MedianFilter::MedianFilter(const MedianFilter& source) :
        data(source.data)
    {
    }

    MedianFilter::~MedianFilter()
    {
    }

    void swap(MedianFilter& first, MedianFilter& second)
    {
        swap(first.data, second.data);
    }

    MedianFilter& MedianFilter::operator=(MedianFilter source)
    {
        swap(*this, source);
        return *this;
    }

    double MedianFilter::filter(const double input, const unsigned int channel,
            const unsigned int milliseconds)
    {
        data.addData(input, channel, milliseconds);
        unsigned int length = data.getDataLength();
        return quickSelect(data.getData(channel, false).array(), length,
                length / 2 + 1);
    }

    Filter* MedianFilter::createCopy() const
    {
        MedianFilter* aCopy = new MedianFilter(*this);
        return aCopy;
    }

    unsigned int MedianFilter::getChannels() const
    {
        return data.getChannels();
    }

    unsigned int MedianFilter::getSampleFrequency() const
    {
        return 1000 / getSamplePeriod();
    }

    unsigned int MedianFilter::getSamplePeriod() const
    {
        return data.getUpdatePeriod();
    }

    Str MedianFilter::getDescription() const
    {
        Str result = "Median filter (Duration: " +
                toStr(data.getDataLength() * data.getUpdatePeriod()) + " ms)";
        return result;
    }

    TemporalData MedianFilter::getTemporalData() const
    {
        return data;
    }

    std::istream& operator>>(std::istream& is, MedianFilter& destination)
    {
        TemporalData temporalData;
        is >> temporalData;
        if (is.good())
        {
            destination = MedianFilter(temporalData);
        }
        return is;
    }

    std::ostream& operator<<(std::ostream& os, const MedianFilter& source)
    {
        os << source.getTemporalData();
        return os;
    }
}
