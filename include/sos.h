// Function for creating a second-order sections matrix.
//
// (c) 2017: Marcel Admiraal

#include "vectorcomplex.h"
#include "matrix.h"

namespace ma
{
    /**
     * Creates the second-order sections matrix from the zeros, poles and gain.
     *
     * @param zero  The vector of complex conjugate zero pairs.
     * @param pole  The vector of complex conjugate pole pairs.
     * @param gain  The overall gain of the system. Default: 1
     * @return      The second-order sections matrix.
     */
    Matrix zp2sos(const VectorComplex& zero, const VectorComplex& pole =
            VectorComplex(), const double gain = 1);
}
